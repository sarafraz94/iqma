<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Welcome extends CI_Controller {

    var $hdata;
    public function __construct() {
        parent::__construct();
        $this->hdata =array(
            'academic' => $this->welcome_model->select_all_menu_academics_info(),
            'facilities' => $this->welcome_model->select_all_menu_facilities_info(),
            'admission' => $this->welcome_model->select_all_menu_admission_info(),
            'events' => $this->welcome_model->select_all_menu_events_info() 
        );

    }
    
    
    public function index() {
              
         $hdata=  $this->hdata;
         $hdata['title'] = 'IQMA : Ilmul Quran Muslim Academy';
         $hdata['metakeyword'] = 'ilmul,quran,muslim,academy,faundation,islamic,islam,bangladesh,online,quran,learn,dhaka,web,online';
         $hdata['metadescription'] = 'IQMA : Ilmul Quran Muslim Academy';
         
         $this->load->view('inc/header',$hdata );
       
        
        $data = array();
        $data['pos_home'] = $this->welcome_model->home_pos();

        $this->load->view('home', $data);

        $this->load->view('inc/gallery');
        $this->load->view('inc/footer');
    }

    public function home_details($id) {
         $hdata=  $this->hdata;
         $hdata['title'] = 'IQMA : Ilmul Quran Muslim Academy';
         $hdata['metakeyword'] = 'ilmul,quran,muslim,academy,faundation,islamic,islam,bangladesh,online,quran,learn,dhaka,web,online';
         $hdata['metadescription'] = 'IQMA : Ilmul Quran Muslim Academy';
         
         $this->load->view('inc/header',$hdata );
        $this->load->view('inc/header');
        $data = array();
        $data['home_delails'] = $this->welcome_model->home_details_info($id);
        $this->load->view('home_details', $data);
        $this->load->view('inc/gallery');
        $this->load->view('inc/footer');
    }

    public function about() {
         $hdata=  $this->hdata;
         $hdata['title'] = 'About Us : Ilmul Quran Muslim Academy( IQMA )';
         $hdata['metakeyword'] = 'ilmul,quran,muslim,academy,faundation,islamic,islam,bangladesh,online,quran,learn,dhaka,web,online';
         $hdata['metadescription'] = 'IQMA : Ilmul Quran Muslim Academy';
         $this->load->view('inc/header',$hdata );
         
        $data = array();
        $data['pos_1'] = $this->welcome_model->about_pos_1();
        $data['pos_2'] = $this->welcome_model->about_pos_2();
        $data['pos_3'] = $this->welcome_model->about_pos_3();
        $data['pos_4'] = $this->welcome_model->about_pos_4();
        $this->load->view('modules/about/about', $data);
        $this->load->view('inc/gallery');
        $this->load->view('inc/footer');
    }

    public function details($id) {
         $hdata=  $this->hdata;
         $hdata['title'] = 'IQMA : Ilmul Quran Muslim Academy';
         $hdata['metakeyword'] = 'ilmul,quran,muslim,academy,faundation,islamic,islam,bangladesh,online,quran,learn,dhaka,web,online';
         $hdata['metadescription'] = 'IQMA : Ilmul Quran Muslim Academy';
         
         $this->load->view('inc/header',$hdata );
        $data = array();
        $data['work_delails'] = $this->welcome_model->work_details_info($id);
        $this->load->view('details', $data);
        $this->load->view('inc/gallery');
        $this->load->view('inc/footer');
    }

//        academics +++  academics
//        academics +++  academics
//        academics +++  academics


 
    public function academics($id) {
//        if ($id != '') {
//            $user_id = (int) $id;
//            $data = array(
//                'title' => 'Edit a User',
//                'action' => site_url('users/edit') . '/' . $user_id,
//                'users' => $this->Prime_model->get_data('users', 'id', $user_id),
//                'button' => 'Update'
//            );
//            if (count($data['users']) < 1) {
//                redirect('users');
//            }
//        }
//       else {
//           show_404();
//         //  $this->load->view('notfound');
//       }
      
         $hdata=  $this->hdata;
         $hdata['title'] = 'About Us : Ilmul Quran Muslim Academy( IQMA )';
         $hdata['metakeyword'] = 'ilmul,quran,muslim,academy,faundation,islamic,islam,bangladesh,online,quran,learn,dhaka,web,online';
         $hdata['metadescription'] = 'IQMA : Ilmul Quran Muslim Academy';
         $this->load->view('inc/header',$hdata );
        
        $data['academics_data']=  $this->welcome_model->select_academics_info_by_id($id);
       
//        $aca_data=array();
//        $aca_data['academics_data']=  $this->welcome_model->select_academics_info_by_id($id);
        
        $this->load->view('modules/academics/academics',$data);
        $this->load->view('inc/footer');
    }
    public function facilities() {
        
       if ($this->uri->segment(3) === FALSE) {
           $id = 0;
        } else {
            $id = $this->uri->segment(3);
        } 

        //$id=  $this->input->post('')
         $hdata=  $this->hdata;
         $hdata['title'] = 'About Us : Ilmul Quran Muslim Academy( IQMA )';
         $hdata['metakeyword'] = 'ilmul,quran,muslim,academy,faundation,islamic,islam,bangladesh,online,quran,learn,dhaka,web,online';
         $hdata['metadescription'] = 'IQMA : Ilmul Quran Muslim Academy';
         $this->load->view('inc/header',$hdata );
        
        $fac_data=array();
        $fac_data['facilities_data']=  $this->welcome_model->select_facilities_info_by_id($id);
        
        $this->load->view('modules/facilities/facilities', $fac_data);
        $this->load->view('inc/footer');
    }

    public function admission() {
         $hdata=  $this->hdata;
         $hdata['title'] = 'About Us : Ilmul Quran Muslim Academy( IQMA )';
         $hdata['metakeyword'] = 'ilmul,quran,muslim,academy,faundation,islamic,islam,bangladesh,online,quran,learn,dhaka,web,online';
         $hdata['metadescription'] = 'IQMA : Ilmul Quran Muslim Academy';
         $this->load->view('inc/header',$hdata );
         
         $adm_data=array();
         $adm_data['admission_data']=  $this->welcome_model->select_admission_info_by_id($id);         
         $this->load->view('modules/admission/admission', $adm_data);
         $this->load->view('inc/footer');
    }

    public function events() {
         $hdata=  $this->hdata;
         $hdata['title'] = 'IQMA : Ilmul Quran Muslim Academy';
         $hdata['metakeyword'] = 'ilmul,quran,muslim,academy,faundation,islamic,islam,bangladesh,online,quran,learn,dhaka,web,online';
         $hdata['metadescription'] = 'IQMA : Ilmul Quran Muslim Academy';
         
         $this->load->view('inc/header',$hdata );
        $this->load->view('modules/events/events');
        $this->load->view('inc/footer');
    }



    public function gallery() {
         $hdata=  $this->hdata;
         $hdata['title'] = 'IQMA : Ilmul Quran Muslim Academy';
         $hdata['metakeyword'] = 'ilmul,quran,muslim,academy,faundation,islamic,islam,bangladesh,online,quran,learn,dhaka,web,online';
         $hdata['metadescription'] = 'IQMA : Ilmul Quran Muslim Academy';
         
         $this->load->view('inc/header',$hdata );
        $this->load->view('gallery');
        $this->load->view('inc/footer');
    }

    public function work() {
        $hdata=  $this->hdata;
         $hdata['title'] = 'IQMA : Ilmul Quran Muslim Academy';
         $hdata['metakeyword'] = 'ilmul,quran,muslim,academy,faundation,islamic,islam,bangladesh,online,quran,learn,dhaka,web,online';
         $hdata['metadescription'] = 'IQMA : Ilmul Quran Muslim Academy';
         
         $this->load->view('inc/header',$hdata );
        $data = array();
        $data['all_work'] = $this->welcome_model->select_all_work_info();
        $this->load->view('work', $data);   //$this->load->view('work', $data, TRUE); sofiul sir er niom  //  run korbe na 

        $this->load->view('inc/gallery');
        $this->load->view('inc/footer');
    }
    
    public function notice_board()
    {
         $hdata=  $this->hdata;
         $hdata['title'] = 'IQMA : Ilmul Quran Muslim Academy';
         $hdata['metakeyword'] = 'ilmul,quran,muslim,academy,faundation,islamic,islam,bangladesh,online,quran,learn,dhaka,web,online';
         $hdata['metadescription'] = 'IQMA : Ilmul Quran Muslim Academy';
         
        $this->load->view('inc/header',$hdata );
        $notice_data =array();
        
        $notice_data['notice_data']=$this->welcome_model->select_all_notice_info();
        $this->load->view('modules/notice/notice_board',$notice_data);
        
        $this->load->view('inc/footer');
    }

// ++++++++++===============+++++++++++++++       
// ++++++++++===============+++++++++++++++ 
//        contact contact        
// ++++++++++===============+++++++++++++++       
// ++++++++++===============+++++++++++++++       




    public function contact() {
        $hdata=  $this->hdata;
         $hdata['title'] = 'IQMA : Ilmul Quran Muslim Academy';
         $hdata['metakeyword'] = 'ilmul,quran,muslim,academy,faundation,islamic,islam,bangladesh,online,quran,learn,dhaka,web,online';
         $hdata['metadescription'] = 'IQMA : Ilmul Quran Muslim Academy';
         
        
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        if ($this->input->post('submit')) {



            $this->form_validation->set_rules('username', 'Name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('subject', 'Subject', 'required');
            $this->form_validation->set_rules('message', 'Message', 'required|min_length[200]|max_length[1000]');

            //$this->form_validation->set_message('valid_email', 'This {field} filed is invalid');
            $name = $this->input->post('username', TRUE);
            $email = $this->input->post('email', TRUE);
            $subject = $this->input->post('subject', TRUE);
            $message = $this->input->post('message', TRUE);




            $contact = array(
                'name' => $name,
                'email' => $email,
                'subject' => $subject,
                'message' => $message
            );
            if ($this->form_validation->run()) {
                $this->db->insert('tbl_contact', $contact);
                redirect('welcome/contact');
            } else {
                $this->load->view('inc/header',$hdata );
                $this->load->view('contact');
                $this->load->view('inc/gallery');
                $this->load->view('inc/footer');
                return;
            }
        }
        $this->load->view('inc/header',$hdata );
        $this->load->view('contact');
        $this->load->view('inc/gallery');
        $this->load->view('inc/footer');
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */