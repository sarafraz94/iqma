<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

session_start();
class Admin extends CI_Controller{
        public function __construct() {
        parent::__construct();
        $admin_id=  $this->session->userdata('admin_id');
        if($admin_id != NULL)
        {
            redirect('super_admin','refresh');
        }

    }
    
    
    
     public function index()
	 {	
                        $this->load->view('admin/login');
	 }
         
         
     public function admin_login_check(){
                     $username = $this->input->post('username', TRUE);
                     $password = $this->input->post('password', TRUE);
                     $result = $this->admin_model->admin_login_check_info($username, $password);

                    $sdata=array();
                    if ($result) {

                        $sdata['admin_full_name']=$result->admin_full_name;
                        $sdata['admin_id']=$result->admin_id;
                        $this->session->set_userdata($sdata);
                        redirect('super_admin');
                    } else {
                        $sdata['exception'] = 'Your username and password is wrong';
                        $this->session->set_userdata($sdata);
                        redirect('admin');
                    }
    }
  }

