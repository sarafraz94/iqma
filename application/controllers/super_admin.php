<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
session_start();

class Super_Admin extends CI_Controller{
    
        public function __construct() {
        parent::__construct();
      
        $admin_id = $this->session->userdata('admin_id');

        if ($admin_id == null) {
            redirect('admin', 'refresh');
        }
    }
        

    
    
    
     public function index()
	 {
                                        
		$this->load->view('admin/inc/header');
		$this->load->view('admin/home');
		$this->load->view('admin/inc/footer');
	 }
     public function contact_us()
	 {
		$this->load->view('admin/inc/header');
                $data['all_contact_us'] = $this->super_admin_model->select_all_contact_us();
		$this->load->view('admin/contact_us' , $data);
		$this->load->view('admin/inc/footer');
	 }
      public function logout() 
              {
                $this->session->unset_userdata('admin_id');
                $this->session->unset_userdata('admin_full_name');
                $sdata = array();
                $sdata['message'] = 'You logout successfully';
                $this->session->set_userdata($sdata);
                redirect('admin');
              }
//  ==================+++++++++++++++++++++++++++====================
//  HomePage  *****  HomePage  *****  HomePage  *****  HomePage  ***** HomePage  
//  ==================+++++++++++++++++++++++++++====================             
  public function  manage_home()
   {
        $this->load->view('admin/inc/header');
        
        $data = array();
        $data['all_home'] = $this->super_admin_model->select_all_home();
        $this->load->view('admin/modules/homepage/manage_home', $data);   //$this->load->view('admin/modules/news/manage_news', $data, TRUE); sofiul sir er niom   //  run korbe na 
        
        $this->load->view('admin/inc/footer');
      
  }
  
  public function home_edit($id) {
        $this->load->view('admin/inc/header');
        
        $data =array();
        $data['home_info']=  $this->super_admin_model->select_home_info_by_id($id);
        $this->load->view('admin/modules/homepage/edit_home_form',$data);
        
        $this->load->view('admin/inc/footer');
    }
    
    public function update_home(){
        $home_id=  $this->input->post('home_id',true);
        $data=array();
        $data['home_title']=  $this->input->post('home_title', true);
        $data['home_author_name']=  $this->input->post('home_author_name', true);
        $data['home_short_description']=  $this->input->post('home_short_description', true);
        $data['home_long_description']=  $this->input->post('home_long_description', true);
     
                             
                                /* ------------Update image upload ---------------  */
                                /* ------------ Update image upload ---------------  */
     
  
           if (!empty($_FILES["home_image"]['name'])){                           
                                      $config['upload_path'] = 'uploads/';
                                	$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '1000';  // in KB
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
//		$config['encrypt_name'] = TRUE;   // from struct over flow
                                      $new_name = time().$_FILES["home_image"]['name'];
                                      if ($this->input->post('home_image_old') != null){
                                       $new_name =  $this->input->post('home_image_old');
                                      }
                                     $config['file_name'] = $new_name;
                                     
                                     // delete privious file 
                                     
                                     $path='uploads/'.$new_name;
                                     unlink($path); 
                                    
                                     // delete privious file   
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		$error='';
		$fdata=array();
		
		if ( ! $this->upload->do_upload('home_image'))
		{
			$error = $this->upload->display_errors();

			
		}
		else
		{  
			 $fdata = $this->upload->data();
                                                          if (  $fdata['file_name'] != $this->input->post('home_image_old')){
                                                                         $data['home_image'] =  $fdata['file_name'];
                                                               } 
		}  

                              
                            
        
           }    
  /* ------------ Update image upload ---------------  */
  /* ------------ Update image upload ---------------  */
        

        $this->super_admin_model->update_home_info($home_id, $data);
        redirect('super_admin/manage_home');
    }
 
    
    

    
//  ==================+++++++++++++++++++++++++++====================
//  About Us  *****  About Us  *****  About Us  *****  About Us  ****
//  ==================+++++++++++++++++++++++++++====================
    public function manage_about()
    {
        $this->load->view('admin/inc/header');
        
        $data = array();
        $data['all_about'] = $this->super_admin_model->select_all_about();
        $this->load->view('admin/modules/about/manage_about', $data);   //$this->load->view('admin/modules/news/manage_news', $data, TRUE); sofiul sir er niom   //  run korbe na 
        
        $this->load->view('admin/inc/footer');
    }
      public function about_edit($id) {
        $this->load->view('admin/inc/header');
        
        $data =array();
        $data['about_info']=  $this->super_admin_model->select_about_info_by_id($id);
        $this->load->view('admin/modules/about/edit_about_form',$data);
        
        $this->load->view('admin/inc/footer');
    }
    public function update_about()
    {
        $about_id=  $this->input->post('about_id',true);
        $data=array();
        $data['about_title']=  $this->input->post('about_title', true);
        $data['about_sub_title']=  $this->input->post('about_sub_title', true);
        $data['about_author_name']=  $this->input->post('about_author_name', true);
        $data['about_description']=  $this->input->post('about_description', true);
        $this->super_admin_model->update_about_info($about_id, $data);
        redirect('super_admin/manage_about');
     }
    
    
    
//  ==================+++++++++++++++++++++++++++====================
//  Academics  *****  Academics  *****  Academics  *****  Academics  *****  Academics 
//  ==================+++++++++++++++++++++++++++====================

     public function academics()
     {
     
            $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
           
            if ($this->input->post('submit')) {
                
                $this->form_validation->set_rules('menu_title', 'Manu Title', 'required|min_length[5]|max_length[50]');
                $this->form_validation->set_rules('sub_title', 'Sub Title', 'required|min_length[5]|max_length[50]');
                $this->form_validation->set_rules('title', 'Title', 'required|min_length[10]|max_length[100]');
                $this->form_validation->set_rules('description', 'Description', 'required|min_length[100]');
                
                $data=array();
                
                $data['type_name'] = $this->input->post('type_name', true);
                $data['type_id'] = $this->input->post('type_id', true);
                $data['menu_title'] = $this->input->post('menu_title', true);
                $data['sub_title'] = $this->input->post('sub_title', true);
                $data['title'] = $this->input->post('title', true);
                $data['description'] = $this->input->post('description', true);
               
                
                                 /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */
                                      $config['upload_path'] = 'uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '1000';  // in KB 
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
//		$config['encrypt_name'] = TRUE;   // from struct over flow
                                     $new_name = time().$_FILES["image"]['name'];
                                     $config['file_name'] = $new_name;
                
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		
		$fdata=array();
		
		

                                /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */
                
                
          if ( !$this->form_validation->run() &&  !$this->upload->do_upload('image')) {
               $error = array('error' => $this->upload->display_errors());
               $this->load->view('admin/inc/header');
                $this->load->view('admin/modules/academics/add_academics_form' ,$error);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
          elseif ( !$this->form_validation->run() ||  !$this->upload->do_upload('image')) {
               $error = array('error' => $this->upload->display_errors());
               $this->load->view('admin/inc/header');
                $this->load->view('admin/modules/academics/add_academics_form' ,$error);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
            
            
            else {
                $fdata = $this->upload->data();
                $data['image'] =  $fdata['file_name'];
                $this->db->insert('tbl_academics', $data);
                
                $sdata = array();
                $sdata['afae_message'] = 'You can save Successfully !!';
                $this->session->set_userdata($sdata);
                 redirect('super_admin/academics');
            }
            
            
            
            
            }
            
         $this->load->view('admin/inc/header');
        // $this->load->view('upload_form', array('error' => ' ' ));
         $this->load->view('admin/modules/academics/add_academics_form', array('error' => ' ') );
         $this->load->view('admin/inc/footer');
         
              
     }
     
     
     public function manage_academics(){
         $this->load->view('admin/inc/header');
         $data = array();
         $data['all_academics'] = $this->super_admin_model->select_all_academics();
         
         $this->load->view('admin/modules/academics/manage_academics', $data );
         $this->load->view('admin/inc/footer');
     }
     public function academics_edit($id)
     {
        
        
           $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
           
            if ($this->input->post('submit')) {
                
                $this->form_validation->set_rules('menu_title', 'Manu Title', 'required|min_length[5]|max_length[50]');
                $this->form_validation->set_rules('sub_title', 'Sub Title', 'required|min_length[5]|max_length[50]');
                $this->form_validation->set_rules('title', 'Title', 'required|min_length[10]|max_length[100]');
                $this->form_validation->set_rules('description', 'Description', 'required|min_length[100]');
                
                $data=array();
                
                
                $data['menu_title'] = $this->input->post('menu_title', true);
                $data['sub_title'] = $this->input->post('sub_title', true);
                $data['title'] = $this->input->post('title', true);
                $data['description'] = $this->input->post('description', true);
               
                
                                 /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */
                 if (!empty($_FILES["image"]['name'])){                           
                                      $config['upload_path'] = 'uploads/';
                                	$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '1024';  // in KB
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
//		$config['encrypt_name'] = TRUE;   // from struct over flow
                                      $new_name = time().$_FILES["image"]['name'];
                                      if ($this->input->post('image_old') != null){
                                       $new_name =  $this->input->post('image_old');
                                      }
                                     $config['file_name'] = $new_name;
                                     
                                     // delete privious file 
                                     
                                     $path='uploads/'.$new_name;
                                    
                                    
                                     // delete privious file   
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		
		$fdata=array();
		
		if ( !$this->form_validation->run() &&  !$this->upload->do_upload('image')) {
               $error = array('error' => $this->upload->display_errors());
               $this->load->view('admin/inc/header');
                $data = array();
                $data = $error;
                $data['academics_info']=  $this->super_admin_model->select_academics_info_by_id($id);
                $this->load->view('admin/modules/academics/edit_academics_form' ,$data);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
          elseif ( !$this->form_validation->run() ||  !$this->upload->do_upload('image')) {
                $error = array('error' => $this->upload->display_errors());
                $this->load->view('admin/inc/header');
                $data = array();
                $data = $error;
                $data['academics_info']=  $this->super_admin_model->select_academics_info_by_id($id);
                $this->load->view('admin/modules/academics/edit_academics_form' ,$data);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
            
            
            else {
                
                unlink($path); 
                $fdata = $this->upload->data();
                if (  $fdata['file_name'] != $this->input->post('image_old')){
                $data['image'] =  $fdata['file_name'];}
                $this->db->where('id', $id)->update('tbl_academics', $data);
                
                $sdata = array();
                $sdata['afae_message'] = 'You can edit Successfully !!';
                $this->session->set_userdata($sdata);
                
                 redirect('super_admin/manage_academics');
            }
                            
        
           } 
		
		

                                /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */
                
                
          if ( !$this->form_validation->run()) {
                $this->load->view('admin/inc/header');
                $data = array();
                $data=array('error' => ' ');
                $data['academics_info']=  $this->super_admin_model->select_academics_info_by_id($id);
                $this->load->view('admin/modules/academics/edit_academics_form' ,$data);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
         
            
            else {
                
                $this->db->where('id', $id)->update('tbl_academics', $data);
                
                $sdata = array();
                $sdata['afae_message'] = 'You can save Successfully !!';
                $this->session->set_userdata($sdata);
                
                 redirect('super_admin/manage_academics');
            }
            
            
            
            
            }
            
            
         $this->load->view('admin/inc/header');
         $data = array();
         // $this->load->view('upload_form', array('error' => ' ' ));
         $data=array('error' => ' ');
         $data['academics_info']=  $this->super_admin_model->select_academics_info_by_id($id);
         $this->load->view('admin/modules/academics/edit_academics_form', $data );
         $this->load->view('admin/inc/footer');
         
         
     }
     public function academics_delete($id) {
        if ($id != '') {
            $query =  $this->db->get_where('tbl_academics',array('id' => $id));
            $row = $query->row();
            $picture = $row->image;
            unlink(realpath('uploads/'.$picture));
            
            $this->db->where('id', $id)->delete('tbl_academics');
            redirect('super_admin/manage_academics');
        }
    }
//  ==================+++++++++++++++++++++++++++====================
//  facilities  *****  facilities  *****  facilities  *****  facilities 
//  ==================+++++++++++++++++++++++++++====================

     public function facilities()
     {
     
            $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
           
            if ($this->input->post('submit')) {
                
                $this->form_validation->set_rules('menu_title', 'Manu Title', 'required|min_length[5]|max_length[50]');
                $this->form_validation->set_rules('sub_title', 'Sub Title', 'required|min_length[5]|max_length[50]');
                $this->form_validation->set_rules('title', 'Title', 'required|min_length[10]|max_length[100]');
                $this->form_validation->set_rules('description', 'Description', 'required|min_length[100]');
                
                $data=array();
                
                $data['type_name'] = $this->input->post('type_name', true);
                $data['type_id'] = $this->input->post('type_id', true);
                $data['menu_title'] = $this->input->post('menu_title', true);
                $data['sub_title'] = $this->input->post('sub_title', true);
                $data['title'] = $this->input->post('title', true);
                $data['description'] = $this->input->post('description', true);
               
                
                                 /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */
                                      $config['upload_path'] = 'uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '1000';  // in KB 
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
//		$config['encrypt_name'] = TRUE;   // from struct over flow
                                     $new_name = time().$_FILES["image"]['name'];
                                     $config['file_name'] = $new_name;
                
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		
		$fdata=array();
		
		

                                /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */
                
                
          if ( !$this->form_validation->run() &&  !$this->upload->do_upload('image')) {
               $error = array('error' => $this->upload->display_errors());
               $this->load->view('admin/inc/header');
                $this->load->view('admin/modules/facilities/add_facilities_form' ,$error);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
          elseif ( !$this->form_validation->run() ||  !$this->upload->do_upload('image')) {
               $error = array('error' => $this->upload->display_errors());
                $this->load->view('admin/inc/header');
                $this->load->view('admin/modules/facilities/add_facilities_form' ,$error);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
            
            
            else {
                $fdata = $this->upload->data();
                $data['image'] =  $fdata['file_name'];
                $this->db->insert('tbl_facilities', $data);
                
                $sdata = array();
                $sdata['afae_message'] = 'You can save Successfully !!';
                $this->session->set_userdata($sdata);
                 redirect('super_admin/facilities');
            }
            
            
            
            
            }
            
         $this->load->view('admin/inc/header');
        // $this->load->view('upload_form', array('error' => ' ' ));
         $this->load->view('admin/modules/facilities/add_facilities_form', array('error' => ' ') );
         $this->load->view('admin/inc/footer');
         
              
     }
     
     
     public function manage_facilities(){
         $this->load->view('admin/inc/header');
         $data = array();
         $data['all_facilities'] = $this->super_admin_model->select_all_facilities();
         
         $this->load->view('admin/modules/facilities/manage_facilities', $data );
         $this->load->view('admin/inc/footer');
     }
     public function facilities_edit($id)
     {
        
        
           $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
           
            if ($this->input->post('submit')) {
                
                $this->form_validation->set_rules('menu_title', 'Manu Title', 'required|min_length[5]|max_length[50]');
                $this->form_validation->set_rules('sub_title', 'Sub Title', 'required|min_length[5]|max_length[50]');
                $this->form_validation->set_rules('title', 'Title', 'required|min_length[10]|max_length[100]');
                $this->form_validation->set_rules('description', 'Description', 'required|min_length[100]');
                
                $data=array();
                
                
                $data['menu_title'] = $this->input->post('menu_title', true);
                $data['sub_title'] = $this->input->post('sub_title', true);
                $data['title'] = $this->input->post('title', true);
                $data['description'] = $this->input->post('description', true);
               
                
                                 /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */
                 if (!empty($_FILES["image"]['name'])){                           
                                      $config['upload_path'] = 'uploads/';
                                	$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '1024';  // in KB
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
//		$config['encrypt_name'] = TRUE;   // from struct over flow
                                      $new_name = time().$_FILES["image"]['name'];
                                      if ($this->input->post('image_old') != null){
                                       $new_name =  $this->input->post('image_old');
                                      }
                                     $config['file_name'] = $new_name;
                                     
                                     // delete privious file 
                                     
                                     $path='uploads/'.$new_name;
                                    
                                    
                                     // delete privious file   
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		
		$fdata=array();
		
		if ( !$this->form_validation->run() &&  !$this->upload->do_upload('image')) {
                $error = array('error' => $this->upload->display_errors());
                $this->load->view('admin/inc/header');
                $data = array();
                $data = $error;
                $data['facilities_info']=  $this->super_admin_model->select_facilities_info_by_id($id);
                $this->load->view('admin/modules/facilities/edit_facilities_form' ,$data);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
          elseif ( !$this->form_validation->run() ||  !$this->upload->do_upload('image')) {
                $error = array('error' => $this->upload->display_errors());
                $this->load->view('admin/inc/header');
                $data = array();
                $data = $error;
                $data['facilities_info']=  $this->super_admin_model->select_facilities_info_by_id($id);
                $this->load->view('admin/modules/facilities/edit_facilities_form' ,$data);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
            
            
            else {
                
                unlink($path); 
                $fdata = $this->upload->data();
                if (  $fdata['file_name'] != $this->input->post('image_old')){
                $data['image'] =  $fdata['file_name'];}
                $this->db->where('id', $id)->update('tbl_facilities', $data);
                
                $sdata = array();
                $sdata['afae_message'] = 'You can edit Successfully !!';
                $this->session->set_userdata($sdata);
                
                 redirect('super_admin/manage_facilities');
            }
                            
        
           } 
		
		

                                /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */
                
                
          if ( !$this->form_validation->run()) {
                $this->load->view('admin/inc/header');
                $data = array();
                $data=array('error' => ' ');
                $data['facilities_info']=  $this->super_admin_model->select_facilities_info_by_id($id);
                $this->load->view('admin/modules/facilities/edit_facilities_form' ,$data);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
         
            
            else {
                
                $this->db->where('id', $id)->update('tbl_facilities', $data);
                
                $sdata = array();
                $sdata['afae_message'] = 'You can save Successfully !!';
                $this->session->set_userdata($sdata);
                
                 redirect('super_admin/manage_facilities');
            }
            
            
            
            
            }
            
            
         $this->load->view('admin/inc/header');
         $data = array();
         // $this->load->view('upload_form', array('error' => ' ' ));
         $data=array('error' => ' ');
         $data['facilities_info']=  $this->super_admin_model->select_facilities_info_by_id($id);
         $this->load->view('admin/modules/facilities/edit_facilities_form', $data );
         $this->load->view('admin/inc/footer');
         
         
     }
     
     
     public function facilities_delete($id) {
        if ($id != '') {
            $query =  $this->db->get_where('tbl_facilities',array('id' => $id));
            $row = $query->row();
            $picture = $row->image;
            unlink(realpath('uploads/'.$picture));
            
            $this->db->where('id', $id)->delete('tbl_facilities');
            redirect('super_admin/manage_facilities');
        }
    }

//  ==================+++++++++++++++++++++++++++====================
//  Admission  *****  Admission  *****  Admission  *****  Admission  *****  Admission 
//  ==================+++++++++++++++++++++++++++====================
    
    

     public function admission()
     {
     
            $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
           
            if ($this->input->post('submit')) {
                
                $this->form_validation->set_rules('menu_title', 'Manu Title', 'required|min_length[5]|max_length[50]');
                $this->form_validation->set_rules('sub_title', 'Sub Title', 'required|min_length[5]|max_length[50]');
                $this->form_validation->set_rules('title', 'Title', 'required|min_length[10]|max_length[100]');
                $this->form_validation->set_rules('description', 'Description', 'required|min_length[100]');
                
                $data=array();
                
                $data['type_name'] = $this->input->post('type_name', true);
                $data['type_id'] = $this->input->post('type_id', true);
                $data['menu_title'] = $this->input->post('menu_title', true);
                $data['sub_title'] = $this->input->post('sub_title', true);
                $data['title'] = $this->input->post('title', true);
                $data['description'] = $this->input->post('description', true);
               
                
                                 /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */
                                      $config['upload_path'] = 'uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '1000';  // in KB 
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
//		$config['encrypt_name'] = TRUE;   // from struct over flow
                                     $new_name = time().$_FILES["image"]['name'];
                                     $config['file_name'] = $new_name;
                
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		
		$fdata=array();
		
		

                                /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */
                
                
          if ( !$this->form_validation->run() &&  !$this->upload->do_upload('image')) {
               $error = array('error' => $this->upload->display_errors());
               $this->load->view('admin/inc/header');
                $this->load->view('admin/modules/admission/add_admission_form' ,$error);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
          elseif ( !$this->form_validation->run() ||  !$this->upload->do_upload('image')) {
               $error = array('error' => $this->upload->display_errors());
               $this->load->view('admin/inc/header');
                $this->load->view('admin/modules/admission/add_admission_form' ,$error);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
            
            
            else {
                $fdata = $this->upload->data();
                $data['image'] =  $fdata['file_name'];
                $this->db->insert('tbl_admission', $data);
                
                $sdata = array();
                $sdata['afae_message'] = 'You can save Successfully !!';
                $this->session->set_userdata($sdata);
                 redirect('super_admin/admission');
            }
            
            
            
            
            }
            
         $this->load->view('admin/inc/header');
        // $this->load->view('upload_form', array('error' => ' ' ));
         $this->load->view('admin/modules/admission/add_admission_form', array('error' => ' ') );
         $this->load->view('admin/inc/footer');
         
              
     }
     
     
     public function manage_admission(){
         $this->load->view('admin/inc/header');
         $data = array();
         $data['all_admission'] = $this->super_admin_model->select_all_admission();
         
         $this->load->view('admin/modules/admission/manage_admission', $data );
         $this->load->view('admin/inc/footer');
     }
     public function admission_edit($id)
     {
        
        
           $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
           
            if ($this->input->post('submit')) {
                
                $this->form_validation->set_rules('menu_title', 'Manu Title', 'required|min_length[5]|max_length[50]');
                $this->form_validation->set_rules('sub_title', 'Sub Title', 'required|min_length[5]|max_length[50]');
                $this->form_validation->set_rules('title', 'Title', 'required|min_length[10]|max_length[100]');
                $this->form_validation->set_rules('description', 'Description', 'required|min_length[100]');
                
                $data=array();
                
                
                $data['menu_title'] = $this->input->post('menu_title', true);
                $data['sub_title'] = $this->input->post('sub_title', true);
                $data['title'] = $this->input->post('title', true);
                $data['description'] = $this->input->post('description', true);
               
                
                                 /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */
                 if (!empty($_FILES["image"]['name'])){                           
                                      $config['upload_path'] = 'uploads/';
                                	$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '1024';  // in KB
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
//		$config['encrypt_name'] = TRUE;   // from struct over flow
                                      $new_name = time().$_FILES["image"]['name'];
                                      if ($this->input->post('image_old') != null){
                                       $new_name =  $this->input->post('image_old');
                                      }
                                     $config['file_name'] = $new_name;
                                     
                                     // delete privious file 
                                     
                                     $path='uploads/'.$new_name;
                                    
                                    
                                     // delete privious file   
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		
		$fdata=array();
		
		if ( !$this->form_validation->run() &&  !$this->upload->do_upload('image')) {
               $error = array('error' => $this->upload->display_errors());
               $this->load->view('admin/inc/header');
                $data = array();
                $data = $error;
                $data['admission_info']=  $this->super_admin_model->select_admission_info_by_id($id);
                $this->load->view('admin/modules/admission/edit_admission_form' ,$data);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
          elseif ( !$this->form_validation->run() ||  !$this->upload->do_upload('image')) {
                $error = array('error' => $this->upload->display_errors());
                $this->load->view('admin/inc/header');
                $data = array();
                $data = $error;
                $data['admission_info']=  $this->super_admin_model->select_admission_info_by_id($id);
                $this->load->view('admin/modules/admission/edit_admission_form' ,$data);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
            
            
            else {
                
                unlink($path); 
                $fdata = $this->upload->data();
                if (  $fdata['file_name'] != $this->input->post('image_old')){
                $data['image'] =  $fdata['file_name'];}
                $this->db->where('id', $id)->update('tbl_admission', $data);
                
                $sdata = array();
                $sdata['afae_message'] = 'You can edit Successfully !!';
                $this->session->set_userdata($sdata);
                
                 redirect('super_admin/manage_admission');
            }
                            
        
           } 
		
		

                                /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */
                
                
          if ( !$this->form_validation->run()) {
                $this->load->view('admin/inc/header');
                $data = array();
                $data=array('error' => ' ');
                $data['admission_info']=  $this->super_admin_model->select_admission_info_by_id($id);
                $this->load->view('admin/modules/admission/edit_admission_form' ,$data);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
         
            
            else {
                
                $this->db->where('id', $id)->update('tbl_admission', $data);
                
                $sdata = array();
                $sdata['afae_message'] = 'You can save Successfully !!';
                $this->session->set_userdata($sdata);
                
                 redirect('super_admin/manage_admission');
            }
            
            
            
            
            }
            
            
         $this->load->view('admin/inc/header');
         $data = array();
         // $this->load->view('upload_form', array('error' => ' ' ));
         $data=array('error' => ' ');
         $data['admission_info']=  $this->super_admin_model->select_admission_info_by_id($id);
         $this->load->view('admin/modules/admission/edit_admission_form', $data );
         $this->load->view('admin/inc/footer');
         
         
     }
     public function admission_delete($id) {
        if ($id != '') {
            $query =  $this->db->get_where('tbl_admission',array('id' => $id));
            $row = $query->row();
            $picture = $row->image;
            unlink(realpath('uploads/'.$picture));
            
            $this->db->where('id', $id)->delete('tbl_admission');
            redirect('super_admin/manage_admission');
        }
    }
    
    
    
//  ==================+++++++++++++++++++++++++++====================
//  events  *****  events  *****  events  *****  events  *****  events 
//  ==================+++++++++++++++++++++++++++====================
    
    

     public function events()
     {
     
            $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
           
            if ($this->input->post('submit')) {
                
                $this->form_validation->set_rules('menu_title', 'Manu Title', 'required|min_length[5]|max_length[50]');
                $this->form_validation->set_rules('sub_title', 'Sub Title', 'required|min_length[5]|max_length[50]');
                $this->form_validation->set_rules('title', 'Title', 'required|min_length[10]|max_length[100]');
                $this->form_validation->set_rules('description', 'Description', 'required|min_length[100]');
                
                $data=array();
                
                $data['type_name'] = $this->input->post('type_name', true);
                $data['type_id'] = $this->input->post('type_id', true);
                $data['menu_title'] = $this->input->post('menu_title', true);
                $data['sub_title'] = $this->input->post('sub_title', true);
                $data['title'] = $this->input->post('title', true);
                $data['description'] = $this->input->post('description', true);
               
                
                                 /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */
                                      $config['upload_path'] = 'uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '1000';  // in KB 
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
//		$config['encrypt_name'] = TRUE;   // from struct over flow
                                     $new_name = time().$_FILES["image"]['name'];
                                     $config['file_name'] = $new_name;
                
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		
		$fdata=array();
		
		

                                /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */
                
                
          if ( !$this->form_validation->run() &&  !$this->upload->do_upload('image')) {
               $error = array('error' => $this->upload->display_errors());
               $this->load->view('admin/inc/header');
                $this->load->view('admin/modules/events/add_events_form' ,$error);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
          elseif ( !$this->form_validation->run() ||  !$this->upload->do_upload('image')) {
               $error = array('error' => $this->upload->display_errors());
               $this->load->view('admin/inc/header');
                $this->load->view('admin/modules/events/add_events_form' ,$error);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
            
            
            else {
                $fdata = $this->upload->data();
                $data['image'] =  $fdata['file_name'];
                $this->db->insert('tbl_events', $data);
                
                $sdata = array();
                $sdata['afae_message'] = 'You can save Successfully !!';
                $this->session->set_userdata($sdata);
                 redirect('super_admin/events');
            }
            
            
            
            
            }
            
         $this->load->view('admin/inc/header');
        // $this->load->view('upload_form', array('error' => ' ' ));
         $this->load->view('admin/modules/events/add_events_form', array('error' => ' ') );
         $this->load->view('admin/inc/footer');
         
              
     }
     
     
     public function manage_events(){
         $this->load->view('admin/inc/header');
         $data = array();
         $data['all_events'] = $this->super_admin_model->select_all_events();
         
         $this->load->view('admin/modules/events/manage_events', $data );
         $this->load->view('admin/inc/footer');
     }
     public function events_edit($id)
     {
        
        
           $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
           
            if ($this->input->post('submit')) {
                
                $this->form_validation->set_rules('menu_title', 'Manu Title', 'required|min_length[5]|max_length[50]');
                $this->form_validation->set_rules('sub_title', 'Sub Title', 'required|min_length[5]|max_length[50]');
                $this->form_validation->set_rules('title', 'Title', 'required|min_length[10]|max_length[100]');
                $this->form_validation->set_rules('description', 'Description', 'required|min_length[100]');
                
                $data=array();
                
                
                $data['menu_title'] = $this->input->post('menu_title', true);
                $data['sub_title'] = $this->input->post('sub_title', true);
                $data['title'] = $this->input->post('title', true);
                $data['description'] = $this->input->post('description', true);
               
                
                                 /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */
                 if (!empty($_FILES["image"]['name'])){                           
                                      $config['upload_path'] = 'uploads/';
                                	$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '1024';  // in KB
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
//		$config['encrypt_name'] = TRUE;   // from struct over flow
                                      $new_name = time().$_FILES["image"]['name'];
                                      if ($this->input->post('image_old') != null){
                                       $new_name =  $this->input->post('image_old');
                                      }
                                     $config['file_name'] = $new_name;
                                     
                                     // delete privious file 
                                     
                                     $path='uploads/'.$new_name;
                                    
                                    
                                     // delete privious file   
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		
		$fdata=array();
		
		if ( !$this->form_validation->run() &&  !$this->upload->do_upload('image')) {
               $error = array('error' => $this->upload->display_errors());
               $this->load->view('admin/inc/header');
                $data = array();
                $data = $error;
                $data['events_info']=  $this->super_admin_model->select_events_info_by_id($id);
                $this->load->view('admin/modules/events/edit_events_form' ,$data);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
          elseif ( !$this->form_validation->run() ||  !$this->upload->do_upload('image')) {
                $error = array('error' => $this->upload->display_errors());
                $this->load->view('admin/inc/header');
                $data = array();
                $data = $error;
                $data['events_info']=  $this->super_admin_model->select_events_info_by_id($id);
                $this->load->view('admin/modules/events/edit_events_form' ,$data);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
            
            
            else {
                
                unlink($path); 
                $fdata = $this->upload->data();
                if (  $fdata['file_name'] != $this->input->post('image_old')){
                $data['image'] =  $fdata['file_name'];}
                $this->db->where('id', $id)->update('tbl_events', $data);
                
                $sdata = array();
                $sdata['afae_message'] = 'You can edit Successfully !!';
                $this->session->set_userdata($sdata);
                
                 redirect('super_admin/manage_events');
            }
                            
        
           } 
		
		

                                /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */
                
                
          if ( !$this->form_validation->run()) {
                $this->load->view('admin/inc/header');
                $data = array();
                $data=array('error' => ' ');
                $data['events_info']=  $this->super_admin_model->select_events_info_by_id($id);
                $this->load->view('admin/modules/events/edit_events_form' ,$data);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
         
            
            else {
                
                $this->db->where('id', $id)->update('tbl_events', $data);
                
                $sdata = array();
                $sdata['afae_message'] = 'You can save Successfully !!';
                $this->session->set_userdata($sdata);
                
                 redirect('super_admin/manage_events');
            }
            
            
            
            
            }
            
            
         $this->load->view('admin/inc/header');
         $data = array();
         // $this->load->view('upload_form', array('error' => ' ' ));
         $data=array('error' => ' ');
         $data['events_info']=  $this->super_admin_model->select_events_info_by_id($id);
         $this->load->view('admin/modules/events/edit_events_form', $data );
         $this->load->view('admin/inc/footer');
         
         
     }
     public function events_delete($id) {
        if ($id != '') {
            $query =  $this->db->get_where('tbl_events',array('id' => $id));
            $row = $query->row();
            $picture = $row->image;
            unlink(realpath('uploads/'.$picture));
            
            $this->db->where('id', $id)->delete('tbl_events');
            redirect('super_admin/manage_events');
        }
    }
    
    //  ==================+++++++++++++++++++++++++++====================
//  notice  *****  notice  *****  notice  *****  notice  *****  notice 
//  ==================+++++++++++++++++++++++++++====================
    
    

     public function notice()
     {
     
            $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
           
            if ($this->input->post('submit')) {
                
             
                $this->form_validation->set_rules('description', 'Description', 'required|min_length[20]');
                
                $data=array();
                
                $data['pub_date'] = $this->input->post('pub_date', true);
               
                $data['dead_date'] = $this->input->post('dead_date', true);
                $data['description'] = $this->input->post('description', true);
               
                
                                 /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */
                                      $config['upload_path'] = 'uploads/';
		$config['allowed_types'] = 'pdf';
		$config['max_size']	= '2048';  // in KB 
		
//		$config['encrypt_name'] = TRUE;   // from struct over flow
                                     $new_name = time().$_FILES["image"]['name'];
                                     $config['file_name'] = $new_name;
                
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		
		$fdata=array();
		
		

                                /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */
                
                
          if ( !$this->form_validation->run() &&  !$this->upload->do_upload('image')) {
               $error = array('error' => $this->upload->display_errors());
               $this->load->view('admin/inc/header');
                $this->load->view('admin/modules/notice/add_notice_form' ,$error);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
          elseif ( !$this->form_validation->run() ||  !$this->upload->do_upload('image')) {
               $error = array('error' => $this->upload->display_errors());
               $this->load->view('admin/inc/header');
                $this->load->view('admin/modules/notice/add_notice_form' ,$error);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
            
            
            else {
                $fdata = $this->upload->data();
                $data['image'] =  $fdata['file_name'];
                $this->db->insert('tbl_notice', $data);
                
                $sdata = array();
                $sdata['afae_message'] = 'You can save Successfully !!';
                $this->session->set_userdata($sdata);
                 redirect('super_admin/notice');
            }
            
            
            
            
            }
            
         $this->load->view('admin/inc/header');
        // $this->load->view('upload_form', array('error' => ' ' ));
         $this->load->view('admin/modules/notice/add_notice_form', array('error' => ' ') );
         $this->load->view('admin/inc/footer');
         
              
     }
     
     
     public function manage_notice(){
         $this->load->view('admin/inc/header');
         $data = array();
         $data['all_notice'] = $this->super_admin_model->select_all_notice();
         
         $this->load->view('admin/modules/notice/manage_notice', $data );
         $this->load->view('admin/inc/footer');
     }
     public function notice_edit($id)
     {
        
        
           $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
           
            if ($this->input->post('submit')) {
                
                $this->form_validation->set_rules('description', 'Description', 'required|min_length[20]');
                
                $data=array();
                
                $data['pub_date'] = $this->input->post('pub_date', true);
               
                $data['dead_date'] = $this->input->post('dead_date', true);
                $data['description'] = $this->input->post('description', true);
               
                
                                 /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */
                 if (!empty($_FILES["image"]['name'])){                           
                                      $config['upload_path'] = 'uploads/';
                                	$config['allowed_types'] = 'pdf';
		$config['max_size']	= '2048';  // in KB
		
//		$config['encrypt_name'] = TRUE;   // from struct over flow
                                      $new_name = time().$_FILES["image"]['name'];
                                      if ($this->input->post('image_old') != null){
                                       $new_name =  $this->input->post('image_old');
                                      }
                                     $config['file_name'] = $new_name;
                                     
                                     // delete privious file 
                                     
                                     $path='uploads/'.$new_name;
                                    
                                    
                                     // delete privious file   
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		
		$fdata=array();
		
		if ( !$this->form_validation->run() &&  !$this->upload->do_upload('image')) {
               $error = array('error' => $this->upload->display_errors());
               $this->load->view('admin/inc/header');
                $data = array();
                $data = $error;
                $data['notice_info']=  $this->super_admin_model->select_notice_info_by_id($id);
                $this->load->view('admin/modules/notice/edit_notice_form' ,$data);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
          elseif ( !$this->form_validation->run() ||  !$this->upload->do_upload('image')) {
                $error = array('error' => $this->upload->display_errors());
                $this->load->view('admin/inc/header');
                $data = array();
                $data = $error;
                $data['notice_info']=  $this->super_admin_model->select_notice_info_by_id($id);
                $this->load->view('admin/modules/notice/edit_notice_form' ,$data);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
            
            
            else {
                
                unlink($path); 
                $fdata = $this->upload->data();
                if (  $fdata['file_name'] != $this->input->post('image_old')){
                $data['image'] =  $fdata['file_name'];}
                $this->db->where('id', $id)->update('tbl_notice', $data);
                
                $sdata = array();
                $sdata['afae_message'] = 'You can edit Successfully !!';
                $this->session->set_userdata($sdata);
                
                 redirect('super_admin/manage_notice');
            }
                            
        
           } 
		
		

                                /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */
                
                
          if ( !$this->form_validation->run()) {
                $this->load->view('admin/inc/header');
                $data = array();
                $data=array('error' => ' ');
                $data['notice_info']=  $this->super_admin_model->select_notice_info_by_id($id);
                $this->load->view('admin/modules/notice/edit_notice_form' ,$data);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
         
            
            else {
                
                $this->db->where('id', $id)->update('tbl_notice', $data);
                
                $sdata = array();
                $sdata['afae_message'] = 'You can save Successfully !!';
                $this->session->set_userdata($sdata);
                
                 redirect('super_admin/manage_notice');
            }
            
            
            
            
            }
            
            
         $this->load->view('admin/inc/header');
         $data = array();
         // $this->load->view('upload_form', array('error' => ' ' ));
         $data=array('error' => ' ');
         $data['notice_info']=  $this->super_admin_model->select_notice_info_by_id($id);
         $this->load->view('admin/modules/notice/edit_notice_form', $data );
         $this->load->view('admin/inc/footer');
         
         
     }
     public function notice_delete($id) {
        if ($id != '') {
            $query =  $this->db->get_where('tbl_notice',array('id' => $id));
            $row = $query->row();
            $picture = $row->image;
            unlink(realpath('uploads/'.$picture));
            
            $this->db->where('id', $id)->delete('tbl_notice');
            redirect('super_admin/manage_notice');
        }
    }
    

    
    
    
    
    
    
    
    
    
    
    
    
    
//  ==================+++++++++++++++++++++++++++====================
//  WORK  *****  WORK  *****  WORK  *****  WORK  *****  WORK  *****  WORK  *****   
//  ==================+++++++++++++++++++++++++++====================
     public function add_work()
	 {
		$this->load->view('admin/inc/header');
                                      $data = array();
        
                                      $data['all_publish_category'] = $this->welcome_model->select_all_publish_category();
		$this->load->view('admin/modules/work/add_work_form', $data);
		$this->load->view('admin/inc/footer');
	 }
     public function save_work()
                   {
                                $data=array();
                                $data['work_title'] = $this->input->post('work_title', true);
                                $data['category_id'] = $this->input->post('category_id', true);
                                $data['work_author_name'] = $this->input->post('work_author_name', true);
                                $data['work_short_description'] = $this->input->post('work_short_description', true);
                                $data['work_long_description'] = $this->input->post('work_long_description', true);                                
                                $data['publication_status'] = $this->input->post('publication_status', true);
                                /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */
                                      $config['upload_path'] = 'uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '1000';  // in KB 
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
//		$config['encrypt_name'] = TRUE;   // from struct over flow
                                     $new_name = time().$_FILES["work_image"]['name'];
                                     $config['file_name'] = $new_name;
                
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		$error='';
		$fdata=array();
		
		if ( ! $this->upload->do_upload('work_image'))
		{
			$error = $this->upload->display_errors();

			
		}
		else
		{
			 $fdata = $this->upload->data();
                                                           $data['work_image'] =  $fdata['file_name'];
		}  

                                /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */

                                $this->super_admin_model->save_work_info($data);
                               
                                
                                $sdata = array();
                                $sdata['work_message'] = 'Your new work save successfully';
                                $this->session->set_userdata($sdata);
                                redirect('super_admin/add_work');
                   }
public function manage_work() 
{
        $this->load->view('admin/inc/header');
        
        $data = array();
        $data['all_work'] = $this->super_admin_model->select_all_work();
        $this->load->view('admin/modules/work/manage_work', $data);   //$this->load->view('admin/modules/news/manage_news', $data, TRUE); sofiul sir er niom   //  run korbe na 
        
        $this->load->view('admin/inc/footer');
}

    public function work_unpublished($work_id) {
        $this->super_admin_model->update_unpublish_work($work_id);
        redirect('super_admin/manage_work');
    }

    public function work_published($work_id) {
        $this->super_admin_model->update_publish_work($work_id);
        redirect('super_admin/manage_work');
    }
    
    
    public function work_edit($work_id) {
        $this->load->view('admin/inc/header');
        
        $data =array();
        $data['work_info']=  $this->super_admin_model->select_work_info_by_id($work_id);
        $this->load->view('admin/modules/work/edit_work_form',$data);
        
        $this->load->view('admin/inc/footer');
    }
    
    public function update_work(){
        $work_id=  $this->input->post('work_id',true);
        $data=array();
        $data['work_title']=  $this->input->post('work_title', true);
        $data['work_author_name']=  $this->input->post('work_author_name', true);
        $data['work_short_description']=  $this->input->post('work_short_description', true);
        $data['work_long_description']=  $this->input->post('work_long_description', true);
        $data['publication_status']=  $this->input->post('publication_status', true);
                             
                                /* ------------Update image upload ---------------  */
                                /* ------------ Update image upload ---------------  */
     
  
           if (!empty($_FILES["work_image"]['name'])){                           
                                      $config['upload_path'] = 'uploads/';
                                	$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '1000';  // in KB
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
//		$config['encrypt_name'] = TRUE;   // from struct over flow
                                      $new_name = time().$_FILES["work_image"]['name'];
                                      if ($this->input->post('work_image_old') != null){
                                       $new_name =  $this->input->post('work_image_old');
                                      }
                                     $config['file_name'] = $new_name;
                                     
                                     // delete privious file 
                                     
                                     $path='uploads/'.$new_name;
                                     unlink($path); 
                                    
                                     // delete privious file   
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		$error='';
		$fdata=array();
		
		if ( ! $this->upload->do_upload('work_image'))
		{
			$error = $this->upload->display_errors();

			
		}
		else
		{  
			 $fdata = $this->upload->data();
                                                          if (  $fdata['file_name'] != $this->input->post('work_image_old')){
                                                                         $data['work_image'] =  $fdata['file_name'];
                                                               } 
		}  

                              
                            
        
           }    
  /* ------------ Update image upload ---------------  */
  /* ------------ Update image upload ---------------  */
        

        $this->super_admin_model->update_work_info($work_id,$data);
        redirect('super_admin/manage_work');
    }
    public function work_delete($work_id){
        $this->super_admin_model->delete_work_info($work_id);
        redirect('super_admin/manage_work');        
    }

    
//  ==================+++++++++++++++++++++++++++====================
//  CATEGORY  *****  CATEGORY  *****  CATEGORY  *****  CATEGORY  *****   
//  ==================+++++++++++++++++++++++++++==================== 
  
     public function add_category()
    {
                             $this->load->view('admin/inc/header');
                             $this->load->view('admin/modules/category/add_category_form');
	          $this->load->view('admin/inc/footer');
    }   
    public function save_category()
   {
        $data=array();
        $data['category_name']=  $this->input->post('category_name', true);        
        $data['category_description']=  $this->input->post('category_description', true);        
        $data['publication_status']=  $this->input->post('publication_status', true);       
       
        $this->super_admin_model->save_category_info($data);
        
        
        $sdata = array();
        $sdata['cat_message'] = 'Category save successfully';
        $this->session->set_userdata($sdata);
         redirect('super_admin/add_category');
    }
   public function manage_category()
   {
        $this->load->view('admin/inc/header');
        
        $data = array();
        $data['all_category'] = $this->super_admin_model->select_all_category();
        $this->load->view('admin/modules/category/manage_category', $data);   //$this->load->view('admin/modules/news/manage_news', $data, TRUE); sofiul sir er niom   //  run korbe na 
        
        $this->load->view('admin/inc/footer');
   }
   
       public function category_unpublished($id) {
        $this->super_admin_model->update_unpublish_category($id);
        redirect('super_admin/manage_category');
    }

    public function category_published($id) {
        $this->super_admin_model->update_publish_category($id);
        redirect('super_admin/manage_category');
    }
        public function category_edit($id) {
        $this->load->view('admin/inc/header');
        
        $data =array();
        $data['category_info']=  $this->super_admin_model->select_category_info_by_id($id);
        $this->load->view('admin/modules/category/edit_category_form',$data);
        
        $this->load->view('admin/inc/footer');
    }
        public function update_category()
   {
        $id=  $this->input->post('id');
        $data=array();
        $data['category_name']=  $this->input->post('category_name', true);        
        $data['category_description']=  $this->input->post('category_description', true);        
        $data['publication_status']=  $this->input->post('publication_status', true);       
       
        $this->super_admin_model->update_category_info($id, $data);
        
        
        $sdata = array();
        $sdata['cat_message'] = 'Category update successfully';
        $this->session->set_userdata($sdata);
         redirect('super_admin/manage_category');
    }
        public function category_delete($id)
     {
        $this->super_admin_model->delete_category_info($id);
        redirect('super_admin/manage_category');        
    }
    
//  ==================+++++++++++++++++++++++++++====================
//  PDF PROJECT  ++++  PDF PROJECT  +++++  PDF PROJECT   ++++  PDF PROJECT   
// ==================+++++++++++++++++++++++++++=====================   
      public function add_project()
    {
                             $this->load->view('admin/inc/header');
                             $this->load->view('admin/modules/project/add_project_form');
	          $this->load->view('admin/inc/footer');
    }
    public function save_project()
    {
        $data=array();
        $data['project_title']=$this->input->post('project_title', true);
        $data['project_description']=  $this->input->post('project_description', true);
        $data['project_status']= $this->input->post('project_status', true);
        $data['publication_status']= $this->input->post('publication_status', true);
                                /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */
                                      $config['upload_path'] = 'uploads/';
		$config['allowed_types'] = 'pdf';
		$config['max_size']	= '6000';  // in KB 
		
//		$config['encrypt_name'] = TRUE;   // from struct over flow
                                     $new_name = time().$_FILES["project_pdf"]['name'];
                                     $config['file_name'] = $new_name;
                
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		$error='';
		$fdata=array();
		
		if ( ! $this->upload->do_upload('project_pdf'))
		{
			$error = $this->upload->display_errors();

			
		}
		else
		{
			 $fdata = $this->upload->data();
                                                           $data['project_pdf'] =  $fdata['file_name'];
		}  

                                /* ------------ PDF upload ---------------  */
                                /* ------------ PDF upload ---------------  */
                
       $this->super_admin_model->save_project_info($data);
        
        
        $sdata = array();
        $sdata['pro_message'] = 'Project save successfully';
        $this->session->set_userdata($sdata);
        redirect('super_admin/add_project');
        
    }
    
public function manage_project() 
{
        $this->load->view('admin/inc/header');
        
        $data = array();
        $data['all_project'] = $this->super_admin_model->select_all_project();
        $this->load->view('admin/modules/project/manage_project', $data);   //$this->load->view('admin/modules/news/manage_news', $data, TRUE); sofiul sir er niom   //  run korbe na 
        
        $this->load->view('admin/inc/footer');
}
       public function project_unpublished($id) 
    {
        $this->super_admin_model->update_unpublish_project($id);
        redirect('super_admin/manage_project');
    }

    public function project_published($id) 
   {
        $this->super_admin_model->update_publish_project($id);
        redirect('super_admin/manage_project');
    }
   
public function project_download($id)
 {
    
    $this->super_admin_model->download_pdf($id);
  
 }
    


}

