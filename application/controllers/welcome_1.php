<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
            $this->load->view('inc/header');
            $data = array();
            $data['pos_1'] = $this->welcome_model->home_pos_1();
            $data['pos_2'] = $this->welcome_model->home_pos_2();

            $this->load->view('home', $data);

            $this->load->view('inc/gallery');
            $this->load->view('inc/footer');
        }
                   public function home_details($id)
                   {
                       	$this->load->view('inc/header');
        $data = array();
        $data['home_delails'] = $this->welcome_model->home_details_info($id);
        $this->load->view('home_details', $data);
        $this->load->view('inc/gallery');
        $this->load->view('inc/footer');
    }
	public function about()
	{
		$this->load->view('inc/header');
                $data = array();
                $data['pos_1'] = $this->welcome_model->about_pos_1();
                $data['pos_2'] = $this->welcome_model->about_pos_2();
                $data['pos_3'] = $this->welcome_model->about_pos_3();
                $data['pos_4'] = $this->welcome_model->about_pos_4();
		$this->load->view('about', $data);
		$this->load->view('inc/gallery');
		$this->load->view('inc/footer');
	}
	public function details($id)
	{
		$this->load->view('inc/header');
                                      $data=array(); 
                                      $data['work_delails']=$this->welcome_model->work_details_info($id);
		$this->load->view('details', $data);
		$this->load->view('inc/gallery');
		$this->load->view('inc/footer');
	}
//	public function contact()
//	{
//		$this->load->view('inc/header');
//		$this->load->view('contact');
//		$this->load->view('inc/gallery');
//		$this->load->view('inc/footer');
//	}
	public function gallery()
	{
		$this->load->view('inc/header');
		$this->load->view('gallery');
		$this->load->view('inc/gallery');
		$this->load->view('inc/footer');
	}
	public function work()
	{
		$this->load->view('inc/header');
                                        $data=array();
                                       $data['all_work']=$this->welcome_model->select_all_work_info();
		 $this->load->view('work', $data);   //$this->load->view('work', $data, TRUE); sofiul sir er niom  //  run korbe na 

		$this->load->view('inc/gallery');
		$this->load->view('inc/footer');
	}
        
// ++++++++++===============+++++++++++++++       
// ++++++++++===============+++++++++++++++ 
//        contact contact        
// ++++++++++===============+++++++++++++++       
// ++++++++++===============+++++++++++++++       
        
        
        
        
        public function contact(){
            $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
            if ($this->input->post('submit')) {
                
                
        //       $this->form_validation->set_message('required', 'The %s field can not be empty');
                
                $this->form_validation->set_rules('username', 'Username',  'callback_username_check');
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
                $this->form_validation->set_rules('subject', 'Subject', 'required');
                $this->form_validation->set_rules('message', 'Message', 'required|min_length[200]|max_length[1000]');

               //$this->form_validation->set_message('valid_email', 'This {field} filed is invalid');
                $username = $this->input->post('username', TRUE);
                $email = $this->input->post('email', TRUE);
                $subject = $this->input->post('subject', TRUE);
                
                
                
                
               $user = array(
                'username' => $username,
                'email' => $email,
                'subject' => $subject
            );
            if ($this->form_validation->run()) {
               
                redirect('welcome');
            } else {
                $this->load->view('inc/header');
		$this->load->view('contact');
		$this->load->view('inc/gallery');
		$this->load->view('inc/footer');
                return;
            }
        }
        $this->load->view('inc/header');
	$this->load->view('contact');
	$this->load->view('inc/gallery');
	$this->load->view('inc/footer');
           
        }
        public function username_check($str)
	{
            if (empty($str)){
                $this->form_validation->set_message('username_check', 'The %s field can not be empty');
			return FALSE;
            }
            
            elseif ($str == 'test')
		{
			$this->form_validation->set_message('username_check', 'The %s field can not be the word "test"');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
        
            
            
            
}
	


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */