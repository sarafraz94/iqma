<?php

class welcome_model extends CI_Model {
    
//    homepage     homepage     homepage     homepage     homepage     homepage     homepage     homepage     homepage     homepage     homepage     homepage 
//    homepage     homepage     homepage     homepage     homepage     homepage     homepage     homepage     homepage     homepage     homepage     homepage 


    
//header header  header header  header header  header header     
    
    public function select_all_menu_academics_info(){
        $this->db->select('id,menu_title,sub_title');
        $query = $this->db->get('tbl_academics');
        $result = $query->result();
        return $result;  
    }
    public function select_all_menu_facilities_info(){
        $this->db->select('id,menu_title,sub_title');
        $query = $this->db->get('tbl_facilities');
        $result = $query->result();
        return $result;  
    }
    public function select_all_menu_admission_info(){
        $this->db->select('id,menu_title,sub_title');
        $query = $this->db->get('tbl_admission');
        $result = $query->result();
        return $result;  
    }
    
    public function select_all_menu_events_info(){
        $this->db->select('id,menu_title,sub_title');
        $query = $this->db->get('tbl_events');
        $result = $query->result();
        return $result;  
    }
    




    public function home_pos()
    {
         $this->db->select('*');
         $query_result = $this->db->get('tbl_home');
         $result = $query_result->result();
         return $result; 
    }

    
   public function home_details_info($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_home');
        $this->db->where('home_id', $id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    
    }
    
//    About Us     About Us     About Us     About Us     About Us     About Us     About Us     About Us     About Us     About Us     About Us
//    About Us     About Us     About Us     About Us     About Us     About Us     About Us     About Us     About Us     About Us     About Us
    
 
    public function about_pos_1()
    {
         $this->db->select('*');
         $this->db->from('tbl_about');
         $this->db->where('about_id', 1);
         $query_result = $this->db->get();
         $result = $query_result->row();
         return $result;
    }
    public function about_pos_2()
    {
         $this->db->select('*');
         $this->db->from('tbl_about');
         $this->db->where('about_id', 2);
         $query_result = $this->db->get();
         $result = $query_result->row();
         return $result; 
    }
    public function about_pos_3()
    {
         $this->db->select('*');
         $this->db->from('tbl_about');
         $this->db->where('about_id', 3);
         $query_result = $this->db->get();
         $result = $query_result->row();
         return $result; 
    }
    public function about_pos_4()
    {
         $this->db->select('*');
         $this->db->from('tbl_about');
         $this->db->where('about_id', 4);
         $query_result = $this->db->get();
         $result = $query_result->row();
         return $result; 
    }


//    academics     academics     academics     academics     academics     academics     academics     academics     academics     academics     academics
//    academics     academics     academics     academics     academics     academics     academics     academics     academics     academics     academics

    public function select_academics_info_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_academics');
        $this->db->where('id', $id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

//    facilities     facilities     facilities     facilities     facilities     facilities     facilities     facilities     facilities     facilities     facilities
//    facilities     facilities     facilities     facilities     facilities     facilities     facilities     facilities     facilities     facilities     facilities
    
    public function select_facilities_info_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_facilities');
        $this->db->where('id', $id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
    
        
//    events     events     events     events     events     events     events     events     events     events     events
//    events     events     events     events     events     events     events     events     events     events     events

    
    
    
    
//    notice board     events     notice board     notice board     notice board     notice board     notice board     notice board     notice board     notice board     notice board
//    notice board     notice board     notice board     notice board     notice board     notice board     notice board     notice board     notice board     notice board     notice board
    
    
    

    
    
    
    
        public function select_all_publish_category(){
        $this->db->select('*');
        $this->db->from('tbl_category');
        $this->db->where('publication_status',1);
        $query_result=  $this->db->get();
        $result=$query_result->result();
        return $result;
        
    }
    public function  select_all_work_info()
    {             //f= font end
         $this->db->select('*');
         $this->db->from('tbl_work');
         $this->db->where('publication_status', 1);
         $query_result = $this->db->get();
         $result = $query_result->result();
         return $result;
    }
    public function work_details_info($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_work');
        $this->db->where('work_id', $id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    
    }
    public function select_all_notice_info(){
        $this->db->select('*');
        $query = $this->db->get('tbl_notice');
        $result = $query->result();
        return $result;  
            }
    public function select_notice_info_by_id($id){
        $this->db->select('*');
        $this->db->from('tbl_notice');
        $this->db->where('id', $id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }           
}
