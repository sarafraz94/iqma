<?php

class Super_Admin_Model extends CI_Model{

        public function save_work_info($data) {
        $this->db->insert('tbl_work', $data);
    }
    
public function  select_all_contact_us()
    {
                    $this->db->select('*');
                    $this->db->from('tbl_contact');
                    $query_result = $this->db->get();
                    $result = $query_result->result();
                    return $result;
   
    }
    
//  ==================+++++++++++++++++++++++++++====================
//  HomePage  *****  HomePage  *****  HomePage  *****  HomePage  ***** HomePage  
//  ==================+++++++++++++++++++++++++++====================         
public function  select_all_home()
    {
                    $this->db->select('*');
                    $this->db->from('tbl_home');
                    $query_result = $this->db->get();
                    $result = $query_result->result();
                    return $result;
   
    }
public function select_home_info_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_home');
        $this->db->where('home_id', $id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    public function update_home_info($home_id, $data)
    {
        $this->db->where('home_id', $home_id);
        $this->db->update('tbl_home', $data);
    }
    
//  ==================+++++++++++++++++++++++++++====================
//  Course  *****  Course  *****  Course  *****  Course  **** Course
//  ==================+++++++++++++++++++++++++++====================
    public function save_course_info($data){
         $this->db->insert('tbl_course', $data); 
    }
    public function select_all_course()
    {
        $this->db->select('*');
        $this->db->from('tbl_course');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
    public function select_course_info_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_course');
        $this->db->where('course_id', $id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    
//  ==================+++++++++++++++++++++++++++====================
//  About Us  *****  About Us  *****  About Us  *****  About Us  ****
//  ==================+++++++++++++++++++++++++++====================
    
    public function  select_all_about()
    {
                    $this->db->select('*');
                    $this->db->from('tbl_about');
                    $query_result = $this->db->get();
                    $result = $query_result->result();
                    return $result;
   
    }
    public function select_about_info_by_id($id)
      {
        $this->db->select('*');
        $this->db->from('tbl_about');
        $this->db->where('about_id', $id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
    
public function update_about_info($about_id, $data)
    {
        $this->db->where('about_id', $about_id);
        $this->db->update('tbl_about', $data);
    }
//  ==================+++++++++++++++++++++++++++====================
//  academics  *****  academics  *****  academics  *****  academics  
//  ==================+++++++++++++++++++++++++++====================

    public  function select_all_academics(){
        $this->db->select('*');
         $this->db->from('tbl_academics');
         $this->db->where('type_id', 1);
         $query_result = $this->db->get();
         $result = $query_result->result();
         return $result; 
    }
    
    public function select_academics_info_by_id($id){
        $this->db->select('*');
        $this->db->from('tbl_academics');
        $this->db->where('id', $id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
    
//  ==================+++++++++++++++++++++++++++====================
//  facilities  *****  facilities  *****  facilities  *****  facilities 
//  ==================+++++++++++++++++++++++++++====================

    public  function select_all_facilities(){
        $this->db->select('*');
         $this->db->from('tbl_facilities');
         $this->db->where('type_id', 2);
         $query_result = $this->db->get();
         $result = $query_result->result();
         return $result; 
    }
    
    public function select_facilities_info_by_id($id){
        $this->db->select('*');
        $this->db->from('tbl_facilities');
        $this->db->where('id', $id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
//  ==================+++++++++++++++++++++++++++====================
//  admission  *****  admission  *****  admission  *****  admission  
//  ==================+++++++++++++++++++++++++++====================
    
        public  function select_all_admission(){
        $this->db->select('*');
         $this->db->from('tbl_admission');
         $this->db->where('type_id', 3);
         $query_result = $this->db->get();
         $result = $query_result->result();
         return $result; 
    }
    
    public function select_admission_info_by_id($id){
        $this->db->select('*');
        $this->db->from('tbl_admission');
        $this->db->where('id', $id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    
//  ==================+++++++++++++++++++++++++++====================
//  events  *****  events  *****  events  *****  events  
//  ==================+++++++++++++++++++++++++++====================
    
        public  function select_all_events(){
        $this->db->select('*');
         $this->db->from('tbl_events');
         $this->db->where('type_id', 4);
         $query_result = $this->db->get();
         $result = $query_result->result();
         return $result; 
    }
    
    public function select_events_info_by_id($id){
        $this->db->select('*');
        $this->db->from('tbl_events');
        $this->db->where('id', $id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
//  ==================+++++++++++++++++++++++++++====================
//  notice  *****  notice  *****  notice  *****  notice  
//  ==================+++++++++++++++++++++++++++====================
    
        public  function select_all_notice(){
        $this->db->select('*');
        $query = $this->db->get('tbl_notice');
        $result = $query->result();
        return $result;  
    }
    
    public function select_notice_info_by_id($id){
        $this->db->select('*');
        $this->db->from('tbl_notice');
        $this->db->where('id', $id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }    
    
    
//  ==================+++++++++++++++++++++++++++====================
//  WORK  *****  WORK  *****  WORK  *****  WORK  *****  WORK  *****  WORK  *****   
//  ==================+++++++++++++++++++++++++++====================
        public function  select_all_work()
                {
                    $this->db->select('*');
                    $this->db->from('tbl_work');
                    $query_result = $this->db->get();
                    $result = $query_result->result();
                    return $result;
   
                }
    public function update_unpublish_work($work_id)
    {
        $this->db->set('publication_status', 0);
        $this->db->where('work_id', $work_id);
        $this->db->update('tbl_work');
    }

    public function update_publish_work($work_id) {
        $this->db->set('publication_status', 1);
        $this->db->where('work_id', $work_id);
        $this->db->update('tbl_work');
    }

    public function select_work_info_by_id($work_id){
        $this->db->select('*');
        $this->db->from('tbl_work');
        $this->db->where('work_id', $work_id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
    public function update_work_info($work_id, $data){
        $this->db->where('work_id', $work_id);
        $this->db->update('tbl_work', $data);
    }
    public function update_category_info($id, $data){
        $this->db->where('category_id', $id);
        $this->db->update('tbl_category', $data);
    }
    
    public function delete_work_info($work_id)
    {
        $query =  $this->db->get_where('tbl_work',array('work_id' => $work_id));
        if( $query->num_rows() > 0 )
            {
                $row = $query->row();
                $picture = $row->work_image;
                unlink(realpath('uploads/'.$picture));
                 $this->db->where('work_id', $work_id);
                 $this->db->delete('tbl_work');
                 return true;
   
                 
             }
             
     return false;
     }
       
//  ==================+++++++++++++++++++++++++++====================
//  CATEGORY  *****  CATEGORY  *****  CATEGORY  *****  CATEGORY  *****   
//  ==================+++++++++++++++++++++++++++====================
     
    public function  save_category_info($data){
        $this->db->insert('tbl_category', $data);
 }
 public function  select_all_category()
{
                    $this->db->select('*');
                    $this->db->from('tbl_category');
                    $query_result = $this->db->get();
                    $result = $query_result->result();
                    return $result;
   
}
   public function update_unpublish_category($id) {
        $this->db->set('publication_status', 0);
        $this->db->where('category_id', $id);
        $this->db->update('tbl_category');
    }

    public function update_publish_category($id) {
        $this->db->set('publication_status', 1);
        $this->db->where('category_id', $id);
        $this->db->update('tbl_category');
    }
        public function select_category_info_by_id($id){
        $this->db->select('*');
        $this->db->from('tbl_category');
        $this->db->where('category_id', $id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
        public function delete_category_info($id)
    {
        $this->db->where('category_id', $id);
        $this->db->delete('tbl_category');
     }

//  ==================+++++++++++++++++++++++++++====================
//  PDF PROJECT  ****  PDF PROJECT  *****  PDF PROJECT   ****  PDF PROJECT   
//  ==================+++++++++++++++++++++++++++====================
 public function save_project_info($data)
 {
     $this->db->insert('tbl_project',$data);
 }
public function  select_all_project()
{
                    $this->db->select('*');
                    $this->db->from('tbl_project');
                    $query_result = $this->db->get();
                    $result = $query_result->result();
                    return $result;
   
}

 public function download_pdf($id)
 {
      $query =  $this->db->get_where('tbl_project',array('project_id' => $id));
        if( $query->num_rows() > 0 )
         {
            $row = $query->row();
            $pdf = $row->project_pdf;
            
            $this->load->helper('download');
            $data = file_get_contents(base_url().'uploads/'.$pdf); // Read the file's contents
            $name = $pdf;

           force_download($name, $data);
           return true;
        }
         return false;
 }
   public function update_unpublish_project($id) {
        $this->db->set('publication_status', 0);
        $this->db->where('project_id', $id);
        $this->db->update('tbl_project');
    }

    public function update_publish_project($id) {
        $this->db->set('publication_status', 1);
        $this->db->where('project_id', $id);
        $this->db->update('tbl_project');
    }
 
 
//   public function update_unpublish_($id) {
//        $this->db->set('publication_status', 0);
//        $this->db->where('work_id', $id);
//        $this->db->update('tbl_work');
//    }
//
//    public function update_publish($id) {
//        $this->db->set('publication_status', 1);
//        $this->db->where('work_id', $id);
//        $this->db->update('tbl_work');
//    }

    
}
