
<div class=" row">
    <div class="col-md-3 col-sm-3 col-xs-6">
        <a data-toggle="tooltip" title="6 new members." class="well top-block" href="#">
            <i class="glyphicon glyphicon-user blue"></i>

            <div>Total Subscriber</div>
            <div>1000</div>
        </a>
    </div>

    <div class="col-md-3 col-sm-3 col-xs-6">
        <a data-toggle="tooltip" title="4 new pro members." class="well top-block" href="#">
            <i class="glyphicon glyphicon-star yellow"></i>

            <div>Pro Members</div>
            <div>228</div>
        </a>
    </div>

   <div class="col-md-3 col-sm-3 col-xs-6">
        <a data-toggle="tooltip" title="12 new messages." class="well top-block" href="#">
            <i class="glyphicon glyphicon-envelope red"></i>

            <div>Messages</div>
            <div>25</div>
        </a>
    </div>
    
     <div class="col-md-3 col-sm-3 col-xs-6">
        <a data-toggle="tooltip" title="34 new sales." class="well top-block" href="<?php echo base_url()?>super_admin/contact_us">
            <i class="glyphicon glyphicon-info-sign green"></i>

            <div>Contact Info </div>
            <div>13320</div>
        </a>
    </div>

    
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-certificate"></i> Introduction</h2>

                <div class="box-icon">
                    
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content row">
                <div class="col-lg-7 col-md-12 center">
                    <h1>WELCOME TO </h1>
                    <h1><b>Ilmul Quran Muslim Academy </b><br>
                        <small>460/4, West Shewrapara, Mirpur, Dhaka-1216</small><br>
                        <small>Email: hmsayediqf@gmail.com</small><br>
                        <small>Mobile: 01842256082</small><br>
                        <small>Web:<a href="<?php echo base_url()?>" target="blank" > www.iqma-bd.org</a></small>
                    </h1>
                    <p>
                        
                    </p>

                    <p class="red"><b>
                            All pages in the menu are functional, take a look at all, please carefully use that.
                     </b></p>


                </div>
                <!-- Ads, you can remove these -->
                <div class="col-lg-5 col-md-12 hidden-xs center-text">
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- sunam Demo 4 -->
                    <ins class="adsbygoogle"
                         style="display:inline-block;width:336px;height:280px"
                         data-ad-client="ca-pub-5108790028230107"
                         data-ad-slot="9467443105"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>

                <div class="col-lg-5 col-md-12 visible-xs center-text">
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- sunam Demo 5 -->
                    <ins class="adsbygoogle"
                         style="display:inline-block;width:250px;height:250px"
                         data-ad-client="ca-pub-5108790028230107"
                         data-ad-slot="8957582309"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
                <!-- Ads end -->

            </div>
        </div>
    </div>
</div>

