
    </div><!--/#content.col-md-0-->
    </div><!--/fluid-row-->



    <hr>

 

    <footer class="row">
        <p class="col-md-9 col-sm-9 col-xs-12 copyright">&copy; 
            <a href="<?php echo base_url()?>" target="_blank" >Ilmul Quran Muslim Academy</a> 2015 - 2016</p>

        <p class="col-md-3 col-sm-3 col-xs-12 powered-by">Developed by: <a
                href="https://www.facebook.com/sarafraz94" target="_blank" style="font-size: 16px;">সুনাম</a></p>
    </footer>

</div><!--/.fluid-container-->

<!-- external javascript -->

<script src="<?php echo base_url()?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="<?php echo base_url()?>js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='<?php echo base_url()?>bower_components/moment/min/moment.min.js'></script>
<script src='<?php echo base_url()?>bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='<?php echo base_url()?>js/jquery.dataTables.min.js'></script>

<!-- select or dropdown enhancer -->
<script src="<?php echo base_url()?>bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="<?php echo base_url()?>bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="<?php echo base_url()?>js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="<?php echo base_url()?>bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="<?php echo base_url()?>bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="<?php echo base_url()?>js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="<?php echo base_url()?>js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="<?php echo base_url()?>js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="<?php echo base_url()?>js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="<?php echo base_url()?>js/jquery.history.js"></script>
<!-- application script for sunam demo -->
<script src="<?php echo base_url()?>js/charisma.js"></script>


<!--My script's-->
<script type='text/javascript'>
            function confirmDelete()
            {
                var chk=confirm("Do you sure want to delete this data?");
                if(chk){
                    return true;
                }
             else{
                 return false ;
             }
            }
</script>




</body>
</html>
