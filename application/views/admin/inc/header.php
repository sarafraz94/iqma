<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <title>Dashbord- Ilmul Quran Muslim Academy </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- The styles -->
    <link  href="<?php echo base_url()?>css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="<?php echo base_url()?>css/charisma-app.css" rel="stylesheet">
    <!--<link href='<?php echo base_url()?>bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>-->
    <!--<link href='<?php echo base_url()?>bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>-->
    <link href='<?php echo base_url()?>bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='<?php echo base_url()?>bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='<?php echo base_url()?>bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='<?php echo base_url()?>bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='<?php echo base_url()?>css/jquery.noty.css' rel='stylesheet'>
    <link href='<?php echo base_url()?>css/noty_theme_default.css' rel='stylesheet'>
    <link href='<?php echo base_url()?>css/elfinder.min.css' rel='stylesheet'>
    <link href='<?php echo base_url()?>css/elfinder.theme.css' rel='stylesheet'>
    <link href='<?php echo base_url()?>css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='<?php echo base_url()?>css/uploadify.css' rel='stylesheet'>
    <link href='<?php echo base_url()?>css/animate.min.css' rel='stylesheet'>
    <!-- Fancybox jQuery -->
    <script type="text/javascript" src="<?php echo base_url(); ?>fancybox/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>fancybox/jquery.fancybox.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>fancybox/main.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>fancybox/jquery.fancybox.css" />
    <!-- //Fancybox jQuery -->

    <!-- CKEditor Start -->
    <script src="//cdn.ckeditor.com/4.5.5/standard/ckeditor.js"></script>
    <!-- // CKEditor End -->

    <!-- jQuery -->
    <script src="<?php echo base_url()?>bower_components/jquery/jquery.min.js"></script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
    <link rel="shortcut icon" href="<?php echo base_url();?>img/fav.png">

</head>

<body>
    <!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url()?>super_admin/index.html"> <img alt="Iqma" src="<?php echo base_url();?>img/logo20.png" class="hidden-xs" />
                <span style="width:;">Ilmul Quran Muslim Academy</span></a>

            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs">  <?php 
                        $admin_name=$this->session->userdata('admin_full_name');
                        $admin_id=$this->session->userdata('admin_id');
                        
                            echo $admin_name;
                        
                        ?></span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#">
                       <?php 
                       echo 'My Profile '. $admin_id;
                       ?>
                        </a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo base_url() ?>super_admin/logout">Log Out</a></li>
                </ul>
            </div>
            <!-- user dropdown ends -->



            <ul class="collapse navbar-collapse nav navbar-nav top-menu">
                <li><a href="<?php echo base_url() ?>"  target="_blank"><i class="glyphicon glyphicon-globe"></i> Visit Site</a></li>

            </ul>

        </div>
    </div>
    <!-- topbar ends -->
<div class="ch-container">
    <div class="row">
        
        <!-- left menu starts -->
        <div class="col-sm-2 col-lg-2">
            <div class="sidebar-nav">
                <div class="nav-canvas">
                    <div class="nav-sm nav nav-stacked">

                    </div>
                    <ul class="nav nav-pills nav-stacked main-menu">
                        <li class="nav-header">Main Menu</li>
                        <li><a class="ajax-link" href="<?php echo base_url()?>super_admin/index.html"><i class="glyphicon glyphicon-th-large"></i><span> Dashboard</span></a></li>

                     

                   <li><a class="ajax-link" href="<?php echo base_url()?>super_admin/manage_home"><i
                                    class="glyphicon glyphicon-home"></i><span> Homepage</span></a></li>
                   <li><a class="ajax-link" href="<?php echo base_url()?>super_admin/manage_about"><i
                                    class="glyphicon glyphicon-user"></i><span> About Us</span></a></li>
                    <li class="accordion"><a href="#"><i class="glyphicon glyphicon-chevron-down"></i><span> Academics</span></a>
                       <ul class="nav nav-pills nav-stacked">
                           <li><a class="ajax-link" href="<?php echo base_url()?>super_admin/manage_academics"><i class="glyphicon glyphicon-list-alt"></i><span> All Academics </span></a> </li>
                           <li><a class="ajax-link" href="<?php echo base_url()?>super_admin/academics"><i class="glyphicon glyphicon-edit"></i><span> New Academics Add</span></a> </li>
                       </ul>
                   </li>
                    <li class="accordion"><a href="#"><i class="glyphicon glyphicon-chevron-down"></i><span> Facilities</span></a>
                       <ul class="nav nav-pills nav-stacked">
                           <li><a class="ajax-link" href="<?php echo base_url()?>super_admin/manage_facilities"><i class="glyphicon glyphicon-list-alt"></i><span> All Facilities </span></a> </li>
                           <li><a class="ajax-link" href="<?php echo base_url()?>super_admin/facilities"><i class="glyphicon glyphicon-edit"></i><span> New Facilities Add</span></a> </li>
                       </ul>
                   </li>
                    <li class="accordion"><a href="#"><i class="glyphicon glyphicon-chevron-down"></i><span> Admission</span></a>
                       <ul class="nav nav-pills nav-stacked">
                           <li><a class="ajax-link" href="<?php echo base_url()?>super_admin/manage_admission"><i class="glyphicon glyphicon-list-alt"></i><span> All Admission </span></a> </li>
                           <li><a class="ajax-link" href="<?php echo base_url()?>super_admin/admission"><i class="glyphicon glyphicon-edit"></i><span> New Admission Add</span></a> </li>
                       </ul>
                   </li>
                    <li class="accordion"><a href="#"><i class="glyphicon glyphicon-chevron-down"></i><span> Events</span></a>
                       <ul class="nav nav-pills nav-stacked">
                           <li><a class="ajax-link" href="<?php echo base_url()?>super_admin/manage_events"><i class="glyphicon glyphicon-list-alt"></i><span> All Events </span></a> </li>
                           <li><a class="ajax-link" href="<?php echo base_url()?>super_admin/events"><i class="glyphicon glyphicon-edit"></i><span> New Events Add</span></a> </li>
                       </ul>
                   </li>
                   
                   <li class="accordion"><a href="#"><i class="glyphicon glyphicon-chevron-down"></i><span> Our Work</span></a>
                       <ul class="nav nav-pills nav-stacked">
                           <li><a class="ajax-link" href="<?php echo base_url()?>super_admin/manage_work"><i class="glyphicon glyphicon-list-alt"></i><span> All Works </span></a> </li>
                           <li><a class="ajax-link" href="<?php echo base_url()?>super_admin/add_work"><i class="glyphicon glyphicon-edit"></i><span> New Work Add</span></a> </li>
                       </ul>
                   </li>
                   
                   
                   
<!--                   <li class="accordion"><a href="#"><i class="glyphicon glyphicon-chevron-down"></i><span> Category's </span></a>
                       <ul class="nav nav-pills nav-stacked">
                           <li><a class="ajax-link" href="<?php echo base_url()?>super_admin/manage_category"><i class="glyphicon glyphicon-list-alt"></i><span> All Category </span></a> </li>
                           <li><a class="ajax-link" href="<?php echo base_url()?>super_admin/add_category"><i class="glyphicon glyphicon-edit"></i><span> New Category Add</span></a> </li>
                        </ul>
                   </li>-->
                   <li class="accordion"><a href="#"><i class="glyphicon glyphicon-chevron-down"></i><span> Notice Board </span></a>
                       <ul class="nav nav-pills nav-stacked">
                           <li><a class="ajax-link" href="<?php echo base_url()?>super_admin/manage_notice"><i class="glyphicon glyphicon-list-alt"></i><span> All Notice  </span></a> </li>
                           <li><a class="ajax-link" href="<?php echo base_url()?>super_admin/notice"><i class="glyphicon glyphicon-edit"></i><span> New Notice Add</span></a> </li>
                       </ul>
                   </li>
                   <li class="accordion"><a href="#"><i class="glyphicon glyphicon-chevron-down"></i><span> Notice Board </span></a>
                       <ul class="nav nav-pills nav-stacked">
                           <li><a class="ajax-link" href="<?php echo base_url()?>super_admin/manage_project"><i class="glyphicon glyphicon-list-alt"></i><span> All Project  </span></a> </li>
                           <li><a class="ajax-link" href="<?php echo base_url()?>super_admin/add_project"><i class="glyphicon glyphicon-edit"></i><span> New Project Add</span></a> </li>
                       </ul>
                   </li>
                        <li><a class="ajax-link" href="gallery.html"><i class="glyphicon glyphicon-picture"></i><span> Gallery</span></a>
                        </li>
                       
                        <li><a href="<?php echo base_url() ?>super_admin/logout"><i class="glyphicon glyphicon-lock"></i><span> Log Out</span></a>
                        </li>
                    </ul>
                    
                </div>
            </div>
        </div>
        <!--/span-->
        <!-- left menu ends -->

       

        <div id="content" class="col-lg-10 col-sm-10">
                        <!-- content starts -->


