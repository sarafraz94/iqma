<!-- content starts -->
<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Edit About Content Form</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i> Edit About Content Form </h2>

                <div class="box-icon">
                    
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form name="edit_about_form" role="form" method="post" action="<?php echo base_url()?>super_admin/update_about"  enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Home Title :</label>
                        
                        <input  type="hidden" name="about_id" value="<?php echo $about_info->about_id?>">
                        
                        <input name="about_title" type="text" class="form-control" id="" placeholder="Enter News Title   !! not more 10 word...   " value="<?php echo $about_info->about_title?>">
                    
                    </div>
                    <div class="form-group">
                        <label>Home Sub Title :</label>
                        
                       
                        
                        <input name="about_sub_title" type="text" class="form-control" id="" placeholder="Enter News Title   !! not more 10 word...   " value="<?php echo $about_info->about_sub_title?>">
                    
                    </div>
                    <div class="form-group">
                        <label>Author Name :</label>
                        <input name="about_author_name" type="text" class="form-control" id="" placeholder="Enter Your Name    !! not more 1 word..." value="<?php echo $about_info->about_author_name?>">
                    </div>

                    
<!--News Long Description-->      <!--News Long Description-->                          
                   <div class="form-group">  
                       <label for="lDes">About Description :</label> 
                
                       
                       <textarea class="ckeditor form-control" id="about_descriptiona" name="about_description" cols="50" rows="10" id="lDes" ><?php echo $about_info->about_description?></textarea>
                    
                      <script>
                         // Replace the <textarea id="editor1"> with a CKEditor
                          // instance, using default configuration.
                       CKEDITOR.replace( 'about_descriptiona' );
                      
                       CKEDITOR.config.height = 500;        // 500 pixels high.
                       CKEDITOR.config.height = '50em';     // CSS unit (em).
                       CKEDITOR.config.width = 500;     // 500 pixels wide.
                       CKEDITOR.config.width = '100%';   // CSS unit (percent).
                       CKEDITOR.config.uiColor = '#2fa4e7';   // CSS unit (percent).
                       
                      </script>
                   
                         </div> 
                    <br><hr>
                
<!--News Long Description-->      <!--News Long Description-->         
                
                
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->

