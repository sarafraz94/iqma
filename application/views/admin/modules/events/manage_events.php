            <!-- content starts -->
       <div>
        <ul class="breadcrumb">
            <li>
                <a href="#">Home</a>
            </li>
            <li>
                <a href="#">All Events</a>
            </li>
        </ul>
    </div>
            
      <?php
            $exc = $this->session->userdata('afae_exception');
            $mes = $this->session->userdata('afae_message');
            if ($exc) {
                echo '<div class="alert alert-danger">' . $exc . '</div>';
                $this->session->unset_userdata('afae_exception');
            } else if ($mes) {
                echo '<div class="alert alert-success">' . $mes . '</div>';
                 $this->session->unset_userdata('afae_message');
            } else {
                echo '<div class="alert alert-info">' . 'Please check carefully.' . '</div>';
            }
           
            
?>

    <div class="row">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-user"></i> All Events View</h2>

        <div class="box-icon">
            <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
            <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a>
            <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
        </div>
    </div>
    <div class="box-content">
 
    <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
    <thead>
    <tr>
        <th>Events ID</th>
        <th>Events Top Menu</th>
        <th>Events Sub Title </th>
        <th>Date </th>
         <th>Actions</th>
    </tr>
    </thead>
    <tbody>
<?php
$i=0;
foreach ($all_events as $v)
    
{
$i++;
?>

    <tr>
        <td><?php echo $i;?></td>
        <td><?php echo $v->menu_title;?></td>
        <td class="center"><?php echo $v->sub_title;?></td>
        <td class="center"><?php echo $v->date;?></td>
        
        <td class="center">
            <a class="btn btn-primary" href="<?php echo base_url().'welcome/events/'.$v->id ;?>" target="_blank"  title="View The post now">
                <i class="glyphicon glyphicon-zoom-in icon-white"></i>
                View
            </a>
                

            <a class="btn btn-info" href="<?php echo base_url() ?>super_admin/events_edit/<?php   echo $v->id;?>">
                <i class="glyphicon glyphicon-edit icon-white"></i>
                Edit
            </a>
            
            <a class="btn btn-danger" href="<?php echo base_url() ?>super_admin/events_delete/<?php   echo $v->id;?>" onclick="return confirmDelete()">
                <i class="glyphicon glyphicon-trash icon-white"></i>
                Delete
            </a>
        </td>
    </tr>
<?php
    }
    ?>
    </tbody>
    </table>
    </div>
    </div>
    </div>
    <!--/span-->

    </div> <!--/row-->