<!-- content starts -->
<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">New Project Add</a>
        </li>
    </ul>
</div>

<?php
            $exc = $this->session->userdata('pro_exception');
            $mes = $this->session->userdata('pro_message');
            if ($exc) {
                echo '<div class="alert alert-danger">' . $exc . '</div>';
                $this->session->unset_userdata('pro_exception');
            } else if ($mes) {
                echo '<div class="alert alert-success">' . $mes . '</div>';
                 $this->session->unset_userdata('pro_message');
            } else {
                echo '<div class="alert alert-info">' . 'Please enter your Project carefully' . '</div>';
            }
           
            
?>  
               

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i> Add Project Form </h2>

                <div class="box-icon">
                    
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form role="form" method="post" action="<?php echo base_url()?>super_admin/save_project" name="" enctype="multipart/form-data">
                      <button type="submit" class="btn btn-primary " style="width: 200px; float: right" >Add Project</button>
                    <div class="form-group">
                        <label>Project Title :</label>
                        <input name="project_title" type="text" class="form-control" id="" placeholder="Project Title  !! " style="width: 400px">
                    </div>
                  

                  <div class="form-group">
                    <label for="sDes">Project Description :</label>
                    <textarea name="project_description" class="form-control" rows="5" id="sDes" placeholder="Write Your Project Description  !!  not more 20 word..." style="width: 400px" ></textarea>
                  </div>
                    
                      
                     <div class="form-group">
                        <label for="exampleInputFile" >News Image :</label>
                        <input name="project_pdf" type="file" id="exampleInputFile">

                        <p class="help-block">Give your PDF for this Project.</p>
                    </div>
                    
     
                

                    <div class="form-group">
                      <label for="sel1">Project Status :</label>
                      <select name="project_status" class="form-control" id="sel1" style="width: 200px" >
                          <option value="1">Upcomming</option>
                          <option value="2">Ongoing</option>
                          <option value="3">Outgoing</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="sel1">Publication Status :</label>
                      <select name="publication_status" class="form-control" id="sel1" style="width: 200px" >
                          <option value="1">Published</option>
                          <option value="0">Unpublished</option>
                      </select>
                    </div>
                    
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->