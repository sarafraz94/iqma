<!-- content starts -->
<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Edit Work Form</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i> Edit Work Form </h2>

                <div class="box-icon">
                    
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form name="edit_work_form" role="form" method="post" action="<?php echo base_url()?>super_admin/update_work"  enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Work Title :</label>
                        <input  type="hidden" name="work_id" value="<?php echo $work_info->work_id?>">
                        <input name="work_title" type="text" class="form-control" id="" placeholder="Enter News Title   !! not more 10 word...   " value="<?php echo $work_info->work_title?>">
                    
                    </div>
                    <div class="form-group">
                        <label>Author Name :</label>
                        <input name="work_author_name" type="text" class="form-control" id="" placeholder="Enter Your Name    !! not more 1 word..." value="<?php echo $work_info->work_author_name?>">
                    </div>
                  <div class="form-group">
                    <label for="sDes">News Short Description :</label>
                    <textarea name="work_short_description" class="form-control" rows="5" id="sDes" placeholder="Write Your News Short Description    !! not more 20 word..."><?php echo $work_info->work_short_description?></textarea>
                  </div>
                    
<!--News Long Description-->      <!--News Long Description-->                          
                   <div class="form-group">  
                       <label for="lDes">News Long Description :</label> 
                
                       
                       <textarea class="form-control" name="work_long_description" cols="50" rows="10" id="lDes" ><?php echo $work_info->work_long_description?></textarea>
 
                            
                      
                   
                </div>   <br><hr>
                
<!--News Long Description-->      <!--News Long Description-->         
                
                
                    <div class="form-group">
                        <label for="exampleInputFile" >News Image Old :</label>
                        <img src="<?php echo base_url().'uploads/'.$work_info->work_image?>" style="height: 230px; width: 370px;">
                        <input  type="hidden" name="work_image_old" value="<?php echo $work_info->work_image?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile" >News Image :</label>
                        <input name="work_image" type="file" id="exampleInputFile">

                        <p class="help-block">Give your Image for this News.</p>
                    </div>
                    <div class="form-group">
                      <label for="sel1">Publication Status :</label>
                      <select name="publication_status" class="form-control" id="sel1">
                          <option value="1">Published</option>
                          <option value="0">Unpublished</option>
                      </select>
                    </div>
                    <button type="submit" class="btn btn-primary ">Submit</button>
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->



            <script type="text/javascript">
            
            document.forms['edit_work_form'].elements['publication_status'].value='<?php echo $work_info->publication_status?>';
            </script>