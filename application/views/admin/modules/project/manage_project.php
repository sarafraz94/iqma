            <!-- content starts -->
      <div>
        <ul class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="">All Project</a>
            </li>
        </ul>
    </div>
            
            

    <div class="row">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-user"></i> All Project View</h2>

        <div class="box-icon">
            <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
            <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a>
            <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
        </div>
    </div>
    <div class="box-content">
     <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
    <thead>
    <tr>
        <th>Project ID</th>
        <th>Work Title</th>
        <th>Project Status</th>
        <th>Status</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
<?php
foreach ($all_project as $v_project)
{

?>

    <tr>
        <td><?php echo $v_project->project_id;?></td>
        <td><?php echo $v_project->project_title;?></td>
        <td class="center">
            <?php 
               
               $pro_status= $v_project->project_status;
               if ($pro_status==1){
                   echo '<span class=" label label-danger">'.'Upcoming'.'</span>';
               }
               elseif ($pro_status==2){
                   echo '<span class=" label label-primary">'.'Ongoing'.'</span>';
               }
               else {
                   echo '<span class=" label label-default">'.'Outgoing'.'</span>';
               }
            ?>
            
          </td>
        
        
        <td class="center">
            
               <?php 
               
               $p_status= $v_project->publication_status;
               if ($p_status==0){
                   echo '<span class="label-default label label-danger">'.'Unpublished'.'</span>';
               }
               else {
                   echo '<span class="label-success label label-default">'.'Published'.'</span>';
               }
            ?>
        </td>
        <td>
            <a class="btn btn-primary" href="<?php echo base_url().'uploads/'.$v_project->project_pdf ;?>" target="_blank"  title="View all project now">
                <i class="glyphicon glyphicon-zoom-in icon-white"></i>
                View
            </a>
              
  <?php 
               
               $p_status= $v_project->publication_status;
               if ($p_status==0){
                echo   ' <a class="btn btn-success" href=" '.base_url().'super_admin/project_published/'.$v_project->project_id .' " title="Published"><i class="glyphicon  glyphicon-plus-sign icon-white"></i></a> ' ;
               }
               else {
              echo  ' <a class="btn btn-danger" href=" '.base_url().'super_admin/project_unpublished/'.$v_project->project_id .' " title="Unpublished"><i class="glyphicon  glyphicon-minus-sign icon-white"></i></a>';
               }
            ?>          
            
            
            

            
            
            <a class="btn btn-info" href="<?php echo base_url() ?>super_admin/project_edit/<?php   echo $v_project->project_id;?>">
                <i class="glyphicon glyphicon-edit icon-white"></i>
                Edit
            </a>
            <a class="btn btn-danger" href="<?php echo base_url() ?>super_admin/project_delete/<?php   echo $v_project->project_id;?>" onclick="return confirmDelete()">
                <i class="glyphicon glyphicon-trash icon-white"></i>
                Delete
            </a>
            <a class="btn btn-danger" href="<?php echo base_url() ?>super_admin/project_download/<?php   echo $v_project->project_id;?>" >
                <i class="glyphicon glyphicon-download icon-white"></i>
                Download
            </a>
            
        </td>
    </tr>
<?php
    }
    ?>
    </tbody>
    </table>
    </div>
    </div>
    </div>
    <!--/span-->

    </div> <!--/row-->