<!-- content starts -->
<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">New Category Add</a>
        </li>
    </ul>
</div>

<?php
            $exc = $this->session->userdata('cat_exception');
            $mes = $this->session->userdata('cat_message');
            if ($exc) {
                echo '<div class="alert alert-danger">' . $exc . '</div>';
                $this->session->unset_userdata('cat_exception');
            } else if ($mes) {
                echo '<div class="alert alert-success">' . $mes . '</div>';
                 $this->session->unset_userdata('cat_message');
            } else {
                echo '<div class="alert alert-info">' . 'Please enter your Category carefully' . '</div>';
            }
           
            
?>  
               

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i> Add Category Form </h2>

                <div class="box-icon">
                    
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form role="form" method="post" action="<?php echo base_url()?>super_admin/save_category" name="" enctype="multipart/form-data">
                      <button type="submit" class="btn btn-primary " style="width: 200px; float: right" >Add Category</button>
                    <div class="form-group">
                        <label>Category Name :</label>
                        <input name="category_name" type="text" class="form-control" id="" placeholder="Category Name  !! " style="width: 200px">
                    </div>
                  

                  <div class="form-group">
                    <label for="sDes">Category Description :</label>
                    <textarea name="category_description" class="form-control" rows="5" id="sDes" placeholder="Write Your Category  Description  !!  not more 20 word..." style="width: 400px" ></textarea>
                  </div>
                    
     
                

                    <div class="form-group">
                      <label for="sel1">Publication Status :</label>
                      <select name="publication_status" class="form-control" id="sel1" style="width: 200px" >
                          <option value="1">Published</option>
                          <option value="0">Unpublished</option>
                      </select>
                    </div>
                    
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->