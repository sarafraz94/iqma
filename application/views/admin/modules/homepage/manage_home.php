            <!-- content starts -->
       <div>
        <ul class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="">Home Content</a>
            </li>
        </ul>
    </div>
            
      <?php
            $exc = $this->session->userdata('work_exception');
            $mes = $this->session->userdata('work_message');
            if ($exc) {
                echo '<div class="alert alert-danger">' . $exc . '</div>';
                $this->session->unset_userdata('work_exception');
            } else if ($mes) {
                echo '<div class="alert alert-success">' . $mes . '</div>';
                 $this->session->unset_userdata('work_message');
            } else {
                echo '<div class="alert alert-info">' . 'Please enter your work carefully' . '</div>';
            }
           
            
?>

    <div class="row">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-user"></i> All Home Content View</h2>

        <div class="box-icon">
            <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
            <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a>
            <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
        </div>
    </div>
    <div class="box-content">
 
    <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
    <thead>
    <tr>
        <th>Home ID</th>
        <th>Home Title</th>
        <th>Position </th>
        <th>Date </th>
        <th>Author</th>
       
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
<?php
foreach ($all_home as $v_home)
{

?>

    <tr>
        <td><?php echo $v_home->home_id;?></td>
        <td><?php echo $v_home->home_title;?></td>
        <td class="center"><?php echo $v_home->home_position;?></td>
        <td class="center"><?php echo $v_home->home_date;?></td>
        <td class="center"><?php echo $v_home->home_author_name;?></td>
        
        <td class="center">
            <a class="btn btn-primary" href="<?php echo base_url().'welcome/home_details/'.$v_home->home_id ;?>" target="_blank"  title="View The post now">
                <i class="glyphicon glyphicon-zoom-in icon-white"></i>
                View
          </a>         
            <a class="btn btn-info" href="<?php echo base_url() ?>super_admin/home_edit/<?php   echo $v_home->home_id;?>">
                <i class="glyphicon glyphicon-edit icon-white"></i>
                Edit
            </a>

        </td>
    </tr>
<?php
    }
    ?>
    </tbody>
    </table>
    </div>
    </div>
    </div>
    <!--/span-->

    </div> <!--/row-->
    