<!-- content starts -->
<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Edit Home Content Form</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i> Edit Home Content Form </h2>

                <div class="box-icon">
                    
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form name="edit_home_form" role="form" method="post" action="<?php echo base_url()?>super_admin/update_home"  enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Home Title :</label>
                        
                        <input  type="hidden" name="home_id" value="<?php echo $home_info->home_id?>">
                        
                        <input name="home_title" type="text" class="form-control" id="" placeholder="Enter News Title   !! not more 10 word...   " value="<?php echo $home_info->home_title?>">
                    
                    </div>
                    <div class="form-group">
                        <label>Author Name :</label>
                        <input name="home_author_name" type="text" class="form-control" id="" placeholder="Enter Your Name    !! not more 1 word..." value="<?php echo $home_info->home_author_name?>">
                    </div>
                  <div class="form-group">
                    <label for="sDes">Home Short Description :</label>
                    <textarea name="home_short_description" class="form-control" rows="5" id="sDes" placeholder="Write Your News Short Description    !! not more 20 word..."><?php echo $home_info->home_short_description?></textarea>
                  </div>
                    
<!--News Long Description-->      <!--News Long Description-->                          
                   <div class="form-group">  
                       <label for="lDes">Home Long Description :</label> 
                
                       
                       <textarea class="form-control" id="home_long_descriptiona" name="home_long_description" cols="50" rows="10" id="lDes" ><?php echo $home_info->home_long_description?></textarea>
                      
                       <script>
                         // Replace the <textarea id="editor1"> with a CKEditor
                          // instance, using default configuration.
                      CKEDITOR.replace( 'home_long_descriptiona' );
                      </script>
                            
                      
                   
                </div>   <br><hr>
                
<!--News Long Description-->      <!--News Long Description-->         
                
                
                    <div class="form-group">
                        <label for="exampleInputFile" >Home Image Old :</label>
                        <img src="<?php echo base_url().'uploads/'.$home_info->home_image?>" style="height: 230px; width: 370px;">
                        
                        <input  type="hidden" name="home_image_old" value="<?php echo $home_info->home_image?>">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputFile" >Home Image :</label>
                        <input name="home_image" type="file" id="exampleInputFile">

                        <p class="help-block">Give your Image for this Home.</p>
                    </div>

                    <button type="submit" class="btn btn-primary ">Submit</button>
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->

