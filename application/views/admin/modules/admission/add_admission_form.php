<!-- content starts -->
<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">New Admission Add</a>
        </li>
    </ul>
</div>


<?php
            $exc = $this->session->userdata('afae_exception');
            $mes = $this->session->userdata('afae_message');
            if ($exc) {
                echo '<div class="alert alert-danger">' . $exc . '</div>';
                $this->session->unset_userdata('afae_exception');
            } else if ($mes) {
                echo '<div class="alert alert-success">' . $mes . '</div>';
                 $this->session->unset_userdata('afae_message');
            } else {
                echo '<div class="alert alert-info">' . 'Please enter your data carefully' . '</div>';
            }
           
            
?>

  <?php  

if (validation_errors() || $error!=' '){
     echo '<div class="alert alert-danger ">'. validation_errors(). $error .' </div>' ;
}
  ?>
  
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i> Add New Admission Form </h2>

                <div class="box-icon">
                    
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form role="form" method="post" action="<?php echo base_url()?>super_admin/admission" name="" enctype="multipart/form-data">
                    <button type="submit" class="btn btn-primary " style="width: 200px; float: right" name="submit" value="submita">Submit</button>
                    <input  type="hidden" name="type_id" value="3">
                    <input  type="hidden" name="type_name" value="admission">
                    <div class="form-group">
                        <label>Top Menu Title :</label>
                        <input name="menu_title" type="text" class="form-control" id="" placeholder="Enter Top Menu Title   !! not more 4 !! word ." value="<?php echo  set_value('menu_title');?>" style="width: 400px">
                             
                    </div>

                    <div class="form-group">
                        <label>Sub Menu Title :</label>
                        <input name="sub_title" type="text" class="form-control" id="" placeholder="Enter Sub Menu Title   !! not more 6 !! word .   " value="<?php echo set_value('sub_title');?>"   style="width: 500px">
                     </div>
                    <div class="form-group">
                        <label>Title :</label>
                        <input name="title" type="text" class="form-control" id="" placeholder="Enter Title   !! not more 10 !! word ." value="<?php echo set_value('title');?>" style="width: 900px">
                    </div>
                                  
                   <div class="form-group">  
                       <label for="lDes">Full Description :</label> 
                
                       
                       <textarea class="form-control" id="admission_descriptiona" name="description" cols="50" rows="10" id="lDes" > <?php echo set_value('description');?></textarea>
                       <script>
                         // Replace the <textarea id="editor1"> with a CKEditor
                          // instance, using default configuration.
                       CKEDITOR.replace( 'description' );
                       CKEDITOR.config.height = 500;        // 500 pixels high.
                       CKEDITOR.config.height = '50em';     // CSS unit (em).
                       CKEDITOR.config.width = 500;     // 500 pixels wide.
                       CKEDITOR.config.width = '100%';   // CSS unit (percent).
                       CKEDITOR.config.uiColor = '#2fa4e7';   // CSS unit (percent).
                      </script>   
                      
                   
                </div>   
                    <hr>
                
<!--News Long Description-->      <!--News Long Description-->         
                
                
                    <div class="form-group">
                        <label for="exampleInputFile" >Suitable Image :</label>
                        <input name="image" type="file" id="exampleInputFile">

                        <p class="help-block">Give Suitable Image .</p>
                    </div>
                    
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->