<!-- content starts -->
<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">New Work Add</a>
        </li>
    </ul>
</div>


<?php
            $exc = $this->session->userdata('work_exception');
            $mes = $this->session->userdata('work_message');
            if ($exc) {
                echo '<div class="alert alert-danger">' . $exc . '</div>';
                $this->session->unset_userdata('work_exception');
            } else if ($mes) {
                echo '<div class="alert alert-success">' . $mes . '</div>';
                 $this->session->unset_userdata('work_message');
            } else {
                echo '<div class="alert alert-info">' . 'Please enter your work carefully' . '</div>';
            }
           
            
?>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i> Add Work Form </h2>

                <div class="box-icon">
                    
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form role="form" method="post" action="<?php echo base_url()?>super_admin/save_work" name="" enctype="multipart/form-data">
                    <button type="submit" class="btn btn-primary " style="width: 200px; float: right">Submit</button>
                    <div class="form-group">
                        <label>Work Title :</label>
                        <input name="work_title" type="text" class="form-control" id="" placeholder="Enter News Title   !! not more 10 word...   " style="width: 500px">
                    </div>
                    <div class="form-group">
                        <label>Author Name :</label>
                        <input name="work_author_name" type="text" class="form-control" id="" placeholder="Enter Your Name    !! not more 1 word..." style="width: 200px">
                    </div>
                  <div class="form-group">
                    <label for="sDes">News Short Description :</label>
                    <textarea name="work_short_description" class="form-control" rows="5" id="sDes" placeholder="Write Your News Short Description    !! not more 20 word..." style="width: 500px"></textarea>
                  </div>
                    
<!--News Long Description-->      <!--News Long Description-->                          
                   <div class="form-group">  
                       <label for="lDes">News Long Description :</label> 
                
                       
                       <textarea class="form-control" id="work_long_descriptiona" name="work_long_description" cols="50" rows="10" id="lDes"  ></textarea>
                       <script>
                         // Replace the <textarea id="editor1"> with a CKEditor
                          // instance, using default configuration.
                      CKEDITOR.replace( 'work_long_descriptiona' );
                      </script>   
                      
                   
                </div>   <br><hr>
                
<!--News Long Description-->      <!--News Long Description-->         
                
                
                    <div class="form-group">
                        <label for="exampleInputFile" >News Image :</label>
                        <input name="work_image" type="file" id="exampleInputFile">

                        <p class="help-block">Give your Image for this News.</p>
                    </div>
                    <div class="form-group">
                      <label for="sel1">Select Work Category :</label>
                      <select name="category_id" class="form-control" id="sel1" style="width: 200px">
                        
                           <?php 
                           foreach ($all_publish_category as $v_category)
                             {
                           ?> 
                                 <option value="<?php echo $v_category->category_id ?>"><?php echo $v_category->category_name ?> </option>
                         <?php 
                                 }
                         ?>  
                      </select>
                    </div>

                    <div class="form-group">
                      <label for="sel1">Publication Status :</label>
                      <select name="publication_status" class="form-control" id="sel1" style="width: 200px">
                          <option value="1">Published</option>
                          <option value="0">Unpublished</option>
                      </select>
                    </div>
                    
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->