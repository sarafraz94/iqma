            <!-- content starts -->
       <div>
        <ul class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="">All Work</a>
            </li>
        </ul>
    </div>
            
      <?php
            $exc = $this->session->userdata('work_exception');
            $mes = $this->session->userdata('work_message');
            if ($exc) {
                echo '<div class="alert alert-danger">' . $exc . '</div>';
                $this->session->unset_userdata('work_exception');
            } else if ($mes) {
                echo '<div class="alert alert-success">' . $mes . '</div>';
                 $this->session->unset_userdata('work_message');
            } else {
                echo '<div class="alert alert-info">' . 'Please enter your work carefully' . '</div>';
            }
           
            
?>

    <div class="row">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-user"></i> All Work View</h2>

        <div class="box-icon">
            <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
            <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a>
            <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
        </div>
    </div>
    <div class="box-content">
 
    <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
    <thead>
    <tr>
        <th>Work ID</th>
        <th>Work Title</th>
        <th>Category </th>
        <th>Date </th>
        <th>Author</th>
        <th>Status</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
<?php
foreach ($all_work as $v_work)
{

?>

    <tr>
        <td><?php echo $v_work->work_id;?></td>
        <td><?php echo $v_work->work_title;?></td>
        <td class="center"><?php echo $v_work->category_id;?></td>
        <td class="center"><?php echo $v_work->work_date;?></td>
        <td class="center"><?php echo $v_work->work_author_name;?></td>
        <td class="center">
            
               <?php 
               
               $p_status= $v_work->publication_status;
               if ($p_status==0){
                   echo '<span class="label-default label label-danger">'.'Unpublished'.'</span>';
               }
               else {
                   echo '<span class="label-success label label-default">'.'Published'.'</span>';
               }
            ?>
        </td>
        <td class="center">
            <a class="btn btn-primary" href="<?php echo base_url().'welcome/details/'.$v_work->work_id ;?>" target="_blank"  title="View The post now">
                <i class="glyphicon glyphicon-zoom-in icon-white"></i>
                View
            </a>
<!--            fancybox start-->


<!--            fancybox End-->
            
            
            
            
            
            
            
            
            
               <?php 
               
               $p_status= $v_work->publication_status;
               if ($p_status==0){
                echo   ' <a class="btn btn-success" href=" '.base_url().'super_admin/work_published/'.$v_work->work_id .' " title="Published"><i class="glyphicon  glyphicon-plus-sign icon-white"></i></a> ' ;
               }
               else {
              echo  ' <a class="btn btn-danger" href=" '.base_url().'super_admin/work_unpublished/'.$v_work->work_id .' " title="Unpublished"><i class="glyphicon  glyphicon-minus-sign icon-white"></i></a>';
               }
            ?>          
            
            
            

            
            
            <a class="btn btn-info" href="<?php echo base_url() ?>super_admin/work_edit/<?php   echo $v_work->work_id;?>">
                <i class="glyphicon glyphicon-edit icon-white"></i>
                Edit
            </a>
            <a class="btn btn-danger" href="<?php echo base_url() ?>super_admin/work_delete/<?php   echo $v_work->work_id;?>" onclick="return confirmDelete()">
                <i class="glyphicon glyphicon-trash icon-white"></i>
                Delete
            </a>
        </td>
    </tr>
<?php
    }
    ?>
    </tbody>
    </table>
    </div>
    </div>
    </div>
    <!--/span-->

    </div> <!--/row-->