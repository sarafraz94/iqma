<!-- content starts -->
<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Edit Notice Form</a>
        </li>
    </ul>
</div>


<?php
            $exc = $this->session->userdata('afae_exception');
            $mes = $this->session->userdata('afae_message');
            if ($exc) {
                echo '<div class="alert alert-danger">' . $exc . '</div>';
                $this->session->unset_userdata('afae_exception');
            } else if ($mes) {
                echo '<div class="alert alert-success">' . $mes . '</div>';
                 $this->session->unset_userdata('afae_message');
            } else {
                echo '<div class="alert alert-info">' . 'Please edit your data carefully.' . '</div>';
            }
           
            
?>

  <?php  

if (validation_errors() || $error!=' '){
     echo '<div class="alert alert-danger ">'. validation_errors(). $error .' </div>' ;
}
  ?>
  
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i> Add New Notice Form </h2>

                <div class="box-icon">
                    
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form role="form" method="post" action="<?php echo base_url().'super_admin/notice_edit/'.$notice_info->id ?>" name="" enctype="multipart/form-data">
                    <button type="submit" class="btn btn-primary " style="width: 200px; float: right" name="submit" value="submita">Submit</button>
                    <input  type="hidden" name="type_id" value="5">
                    <input  type="hidden" name="type_name" value="notice">
                    
                    <div class="form-group">
                        <link href="<?php echo base_url() ?>css/jquery.ui.base.css" rel="stylesheet" type="text/css"/>
                        <link href="<?php echo base_url() ?>css/jquery.ui.theme.css" rel="stylesheet" type="text/css"/>
                        <script src="<?php echo base_url() ?>js/jquery.ui.datepicker.js" type="text/javascript"></script>
                        <script src="<?php echo base_url() ?>js/jquery.ui.core.js" type="text/javascript"></script>
                        <script src="<?php echo base_url() ?>js/jquery.ui.widget.js" type="text/javascript"></script>

                        <script>
                        $(function() {
                                $( "#datepicker" ).datepicker();
                                $( "#datepicker2" ).datepicker();
                        });
                        </script>
                        <p>
                            <b>Publiahed: &nbsp; </b> <input name="pub_date" type="text" id="datepicker" class="form-control" style="width: 120px; display: inline;" value="<?php echo $notice_info->pub_date;?>">
                            <b>&nbsp; Deadline: &nbsp; </b> <input name="dead_date" type="text" id="datepicker2" class="form-control" style="width: 120px; display: inline;" value="<?php echo $notice_info->dead_date;?>">
                        </p>
                    </div>
                                  
                    <div class="form-group">  
                       <label for="lDes">Notice Headline :</label> 
                
                       
                       <textarea class="form-control" id="events_descriptiona" name="description" rows="3" id="lDes" style="width: 500px;" > <?php echo set_value('description');?><?php echo $notice_info->description;?></textarea>
                    </div>  
                    <div class="form-group">
                        <label for="exampleInputFile" >Suitable File :</label>
                        <input name="image" type="file"  id="exampleInputFile">

                        <p class="help-block">Give Suitable File .</p>
                    </div>
                    <hr>
                    
                    <div class="form-group">
                        <label for="exampleInputFile" >News Image Old :</label>
                        <object data="<?php echo base_url().'uploads/'.$notice_info->image?>"  width="100%" style="zoom: 100%;min-height:800px;" >
                            
                        </object>
                        <input  type="hidden" name="image_old" value="<?php echo $notice_info->image?>">
                    </div>
                
<!--News Long Description-->      <!--News Long Description-->         
                
                
                    
                    
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->