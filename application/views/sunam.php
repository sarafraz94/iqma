<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function add_count($slug)

    {
// load cookie helper
    $this->load->helper('cookie');
// this line will return the cookie which has slug name
  $check_visitor = $this->input->cookie(urldecode($slug), FALSE);
// this line will return the visitor ip address
    $ip = $this->input->ip_address();
// if the visitor visit this article for first time then //
 //set new cookie and update article_views column  ..
//you might be notice we used slug for cookie name and ip 
//address for value to distinguish between articles  views
    if ($check_visitor == false) {
        $cookie = array(
            "name"   => urldecode($slug),
            "value"  => "$ip",
            "expire" =>  time() + 7200,
            "secure" => false
        );
        $this->input->set_cookie($cookie);
        $this->welcome_model->update_counter(urldecode($slug));
    }
}



    function update_counter($slug) {
// return current article views 
    $this->db->where('entry_slug', urldecode($slug));
    $this->db->select('entry_views');
    $count = $this->db->get('entry')->row();
// then increase by one 
    $this->db->where('entry_slug', urldecode($slug));
    
    $this->db->set('entry_views', ($count->entry_views + 1));
    $this->db->update('entry');
}