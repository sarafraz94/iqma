


<!--Three images  -->
<section class="featured-services column-view">
    <div class="auto-container">
        <div class="sec-title">

        </div>         


        <div class="row clearfix">

   <?php
   foreach($all_work as $v_work){
   ?>         
            
            
            
            
            
            <article class="col-md-4 col-sm-6 col-xs-12 column-box wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1000ms">
                <div class="inner-box">
                    <figure class="image">
                        <a href="#"><img src="<?php echo base_url()?>uploads/<?php echo $v_work->work_image?>" alt="Image" title="Featured Service"></a>

                    </figure>
                    <div class="post-content clearfix">
                        <h3><a href="<?php base_url()?>details/<?php echo $v_work->work_id ?>"><?php echo $v_work->work_title?></a> </h3>
                        <p>
                           
                             <?php echo $v_work->work_short_description?>

                        </p>
                        <div class="text-center"><a href="<?php base_url()?>details/<?php echo $v_work->work_id ?>" class="theme-btn dark-btn">Read More .. </a></div>
                    </div>

                </div>
            </article>
            
      <?php } ?>         

        </div>

    </div>    
</section>





<section class="why-us">
    <div class="auto-container">
        <div class="row clearfix"> 
            <div class="ia_hometitle">Since 2000, our projects helped millions of people.<br>
                Here are three things that make us different.</div>
            <div class="donateTitle">Donate today, to help save a life</div>


        </div>
    </div>   
</section>    


<!--Donate Section-->

<section class="intro-section theme-two">
    <div class="auto-container">

        <div class="border clearfix">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                <a href="#" class="theme-btn dark-btn style-two">DONATE NOW</a>

            </div>
        </div>
    </div>
</section>
