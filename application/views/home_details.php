    <!-- Sidebar Page -->
    <div class="sidebar-page">
        <div class="auto-container">
            <div class="row clearfix">

                <!-- Left Content -->
                <section class="left-content col-lg-9 col-md-8 col-sm-7 col-xs-12">

                    <!-- Post -->
                    <article class="post post-detail">
                        <div class="post-image wow fadeInUp" data-wow-delay="200ms" data-wow-duration="1500ms">
                            <img class="img-responsive" src="<?php echo base_url(); ?>uploads/<?php echo $home_delails->home_image;?>" alt="" style="height: 350px !important; height: 870px ;">
                         </div>
                        <div class="content-box">
                            <h2 class="post-title"><a href="#"><?php echo $home_delails->home_title;?></a></h2>
                            <div class="post-info">Posted on March 13, 2015 / by <a href="#"><?php echo $home_delails->home_author_name;?></a> / <a href="#">17 comments</a></div>
                            <div class="post-data">
                               
                             <?php echo $home_delails->home_long_description;?>   
                                
                            </div>



                        </div>

                    </article>
                    
           

            <section class="why-us">
                <div class="auto-container">
                    <div class="row clearfix"> 
                        <div class="donateTitle" style="margin:0px 0 20px 0; font-size: 30px;">Donate today, to help save a life</div>

                    </div>
                </div>   
            </section>     
         
                    
            <section class="intro-section theme-two">
                <div class="auto-container">

                    <div class="border clearfix">

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                            <a href="#" class="theme-btn dark-btn style-two">DONATE NOW</a>

                        </div>
                    </div>
                </div>
            </section>

                </section>

                <!-- Side Bar -->

                <div id="sidebar" class="col-md-3" style="float:right;">    
                    
                    
                    
                    <div  class=" widget_nav_menu">
                        <div class="headingsidevar"><h3>Ways to give</h3></div>
                        <div class="">
                            <ul id="menu-ways-to-give" class="menu">
                                <li id="menu-item-2548" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2548"><a href="">Ways to Give</a></li>
                                <li id="menu-item-3276" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3276"><a href="">Donate to Islamic Aid</a></li>
                                <li id="menu-item-2552" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2552"><a href="">Payroll giving</a></li>
                                <li id="menu-item-2553" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2553"><a href="">Fund a project</a></li>
                                <li id="menu-item-2554" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-207 current_page_item menu-item-2554"><a href="">Write an Islamic Will</a></li>
                                <li id="menu-item-2555" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2555"><a href="">Gift aid</a></li>
                                <li id="menu-item-2556" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2556"><a href="">Volunteer for us</a></li>
                                <li id="menu-item-2557" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2557"><a href="">Donate offline</a></li>
                            </ul>
                        </div>
                    </div>



                    <div id="text-11" class="widget ">			

                        <div class=""><div align="center" class="">Any question about our work or your donation?</div>
                            <h3 align="center" class="">Call (UK) 0300 111 3001</h3>

                            <div align="center">calls are included in your free mobile and landline minutes</div>

                            <p align="center">Email:&nbsp; sunam@gmail.com


                            </p>

                            <img src="<?php echo base_url()?>images/resource/securities.png" alt="Securities"/><a href="/how-money-is-spent/"><img src="<?php echo base_url()?>images/resource/donationbanner.png" alt="Securities"/></a></div>
                    </div>


                </div>

            </div>
        </div>
    </div>

