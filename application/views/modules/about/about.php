    <!-- Page Banner -->
    <section class="page-banner" style="background-image:url(<?php echo base_url();?>images/background/page-banner-1.jpg);">
         <div class="auto-container">
             <div class="page-title"><h1><b> ABOUT US </b></h1></div>
            <div class="bread-crumb text-right">
                <span class="initial-text"> </span>
                <a href=""></a>
                <span class=""></span>
            </div>
        </div>
    </section>
    
    
    
    
	<!--Tabs Section-->
    <section class="tabs-section">
    	<div class="auto-container">
        	<div class="row">
            	
                <div class="tabs-box clearfix">
                	
                    <!--Buttons Side-->
                    <div class="col-md-4 col-sm-6 col-xs-12 buttons-side">
                    	
                        <div class="sec-title">
                        	<h3>what we do on our workspace</h3>
                        </div>
                        <!--Tab Buttons-->
                        <ul class="tab-buttons">
                            <li class="wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1000ms"><a href="#tab-one" class="tab-btn active-btn clearfix"><div class="icon"><span class="flaticon-open161"></span></div><h4><?php echo $pos_1->about_title;?></h4><p><?php echo $pos_1->about_sub_title;?></p></a></li>
                            <li class="wow fadeInRight" data-wow-delay="200ms" data-wow-duration="1000ms"><a href="#tab-two" class="tab-btn clearfix"><div class="icon"><span class="flaticon-islam60"></span></div><h4> <?php echo $pos_2->about_title;?> </h4><p><?php echo $pos_2->about_sub_title;?></p></a></li>
                            <li class="wow fadeInRight" data-wow-delay="400ms" data-wow-duration="1000ms"><a href="#tab-three" class="tab-btn clearfix"><div class="icon"><span class="flaticon-bars42"></span></div><h4><?php echo $pos_3->about_title;?></h4><p><?php echo $pos_3->about_sub_title;?></p></a></li>
                            <li class="wow fadeInRight" data-wow-delay="600ms" data-wow-duration="1000ms"><a href="#tab-four" class="tab-btn clearfix"><div class="icon"><span class="flaticon-graduate33"></span></div><h4><?php echo $pos_4->about_title;?></h4><p><?php echo $pos_4->about_sub_title;?></p></a></li>
                        </ul>
                    </div>
                    
                    <!--Content Side-->
                    <div class="col-md-8 col-sm-6 col-xs-12 tabs-content clearfix">
                    	
                        <!--Tab / Active tab-->
                        <div class="tab active-tab" id="tab-one">
                            <div class="tab-title">
                                <h2><?php echo $pos_1->about_title;?></h2>
                                <h3><?php echo $pos_1->about_sub_title;?></h3>
                            </div>
                            <div class="text">
                               <?php echo $pos_1->about_description;?>
                            </div>
                            
          
                            
                        </div>
                        
                        <!--Tab-->
                        <div class="tab" id="tab-two">
                            <div class="tab-title">
                                <h2><?php echo $pos_2->about_title;?></h2>
                                <h3><?php echo $pos_2->about_sub_title;?></h3>
                            </div>
                            <div class="text">
                                <?php echo $pos_2->about_description;?>
                            </div>
                            
                        </div>
                        
                        <!--Tab-->
                        <div class="tab" id="tab-three">
                            <div class="tab-title">
                                <h2><?php echo $pos_3->about_title;?></h2>
                                <h3><?php echo $pos_3->about_sub_title;?></h3>
                            </div>
                            <div class="text">
                                <?php echo $pos_3->about_description;?>
                            </div>
                            
                        </div>
                        
                        <!--Tab-->
                        <div class="tab" id="tab-four">
                            <div class="tab-title">
                                <h2><?php echo $pos_4->about_title;?></h2>
                                <h3><?php echo $pos_4->about_sub_title;?></h3>
                            </div>
                           <div class="text">
                                <?php echo $pos_4->about_description;?>
                            </div>
                            
                        </div>
                        
                    </div>
                    
                </div>
                
            </div>
        </div>
    </section>
    