    <!-- Sidebar Page -->
    <div class="sidebar-page">
        <div class="auto-container">
            <div class="row clearfix">
                                <!-- Side Bar -->

                <div id="sidebar" class="col-md-3" style="float:left;">    
                    
                    
                    
                    <div  class=" widget_nav_menu">
                        <div class="headingsidevar"><h3>ACADEMICS</h3></div>
                        <div class="">
                            <ul id="menu-ways-to-give" class="menu">
                                <li id="menu-item-2548" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2548"><a href="">Ways to Give</a></li>
                                <li id="menu-item-3276" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3276"><a href="">Donate to Islamic Aid</a></li>
                                <li id="menu-item-2552" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2552"><a href="">Payroll giving</a></li>
                                <li id="menu-item-2553" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2553"><a href="">Fund a project</a></li>
                                <li id="menu-item-2554" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-207 current_page_item menu-item-2554"><a href="">Write an Islamic Will</a></li>
                                <li id="menu-item-2555" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2555"><a href="">Gift aid</a></li>
                                <li id="menu-item-2556" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2556"><a href="">Volunteer for us</a></li>
                                <li id="menu-item-2557" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2557"><a href="">Donate offline</a></li>
                            </ul>
                        </div>
                    </div>



                    <div id="text-11" class="widget ">			

                        <div class=""><div align="center" class="">Any question about our work or your donation?</div>
                            <h3 align="center" class="">Call (UK) 0300 111 3001</h3>

                            <div align="center">calls are included in your free mobile and landline minutes</div>

                            <p align="center">Email:&nbsp; sunam@gmail.com


                            </p>

                            <img src="<?php echo base_url()?>images/resource/securities.png" alt="Securities"/><a href="/how-money-is-spent/"><img src="<?php echo base_url()?>images/resource/donationbanner.png" alt="Securities"/></a></div>
                    </div>


                </div>

                <!-- Left Content -->
                <section class="left-content col-lg-9 col-md-8 col-sm-7 col-xs-12">

                    <!-- Post -->
                    <article class="post post-detail">
                        
                        
                        
                        <div class="content-box">
                            <div class="post-image wow fadeInUp" data-wow-delay="200ms" data-wow-duration="1500ms">
                            <img class="img-responsive" src="<?php echo base_url();?>images/resource/blog-image-2.jpg" alt="">
                         </div>
                            <h2 class="post-title"><a href="#">We finished over 2000 car repair jobs in 2014</a></h2>
                            <div class="post-info">Posted on March 13, 2015 / by <a href="#">Jonathan Doe</a> / <a href="#">17 comments</a></div>
                            <div class="post-data">
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</p>
                                
                                
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                                <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat.</p>
                            </div>



                        </div>

                    </article>
                    
           

            <section class="why-us">
                <div class="auto-container">
                    <div class="row clearfix"> 
                        <div class="donateTitle" style="margin:0px 0 20px 0; font-size: 30px;">Donate today, to help save a life</div>


                    </div>
                </div>   
            </section>     
         
                    
            <section class="intro-section theme-two">
                <div class="auto-container">

                    <div class="border clearfix">

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                            <a href="#" class="theme-btn dark-btn style-two">DONATE NOW</a>

                        </div>
                    </div>
                </div>
            </section>

                </section>



            </div>
        </div>
    </div>

