    <!-- Page Banner -->
    <section class="page-banner" style="background-image:url(<?php echo base_url();?>images/background/page-banner-1.jpg);">
         <div class="auto-container">
             <div class="page-title"><h1><b> NOTICE BOARD </b></h1></div>
            <div class="bread-crumb text-right">
                <span class="initial-text"> </span>
                <a href=""></a>
                <span class=""></span>
            </div>
        </div>
    </section>
    
    
    
    
	<!--Tabs Section-->
    <section class="tabs-section">
    	<div class="auto-container">
         <div class="row">
             <div style="margin-left:5%; margin-right: 5%;">
             <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                 <thead>
                     <tr>
                         <th class="center">No:</th>
                         <th class="center">Notice Headline </th>
                         <th class="center">Publish </th>
                         <th class="center">Deadline </th>
                         <th class="center">Actions</th>
                     </tr>
                 </thead>
                 <tbody>
                     <?php
                      $i = 0;
                     foreach ($notice_data as $notice_info) {
                         $i++;
                      ?>

                         <tr>
                             <td class="center"><?php echo $i;?></td>
                             <td class="" style="max-width: 300px;" >
                                <?php echo $notice_info->description;?>
                             </td>
                             <td class="center"><?php echo $notice_info->pub_date;?></td>
                             <td class="center"><?php echo $notice_info->dead_date;?></td>

                             <td class="">
                                 <a class="btn btn-default" href="#<?php echo 'notice'.$i;?>" id="various1"  title="View The post now" style="float: left;">
                                     <i class="glyphicon glyphicon-eye-open icon-white"></i>
                                     View
                                 </a>
                                 <div id="fancybox-content" class="inline1" >
                                    <div id="<?php echo 'notice'.$i;?>"  style="min-width: 800px; min-height: 600px;"> 
                                        <h5 style="padding: 0px 5px 10px 0px;"><?php echo $notice_info->description;?></h5>
                                            <object data="<?php echo base_url().'uploads/'.$notice_info->image?>"  width="100%" style="zoom: 100%;min-height:580px;" >
                                            </object>
                                    </div>
                                 </div> 


                                 &nbsp;
                                 <a class="btn btn-default" href="<?php echo base_url() ?>super_admin/project_download/" style="float: left;" >
                                     <i class="glyphicon glyphicon-download icon-white"></i>
                                     Download
                                 </a>
                             </td>
                         </tr>
                         
                         
    <?php
                     }
?>
                 </tbody>
             </table>  	
               
                
          </div>
          </div>
        </div>
    </section>
    