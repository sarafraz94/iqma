<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        
        
<!--search engine sunam       search engine sunam      search engine sunam-->
<!--search engine sunam       search engine sunam      search engine sunam-->
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta property="og:image" content="http://www.iqma-bd.org/templates/protostar/images/logo.png"/>
        <meta property="og:image" content="http://www.iqma-bd.org/templates/protostar/images/webcoachbd_s.jpg"/>
        <base href="http://www.iqma-bd.org/" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="<?php echo $metakeyword; ?>"/>
        <meta name="description" content="<?php echo $metadescription; ?>" />
        <meta name="generator" content="Iqma! - Ilmul Quran Muslim Academy" />
     
        
        <title><?php echo $title; ?></title>
        
  <link href="http://www.iqma-bd.org/" rel="canonical" />
  <link href="/?format=feed&amp;type=rss" rel="alternate" type="application/rss+xml" title="RSS 2.0" />
  <link href="/?format=feed&amp;type=atom" rel="alternate" type="application/atom+xml" title="Atom 1.0" />
  <link href="/templates/protostar/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
  <link href="http://www.iqma-bd.org/component/search/?format=opensearch" rel="search" title="Search IQMA" type="application/opensearchdescription+xml" />
      
<!--search engine sunam       search engine sunam      search engine sunam-->
<!--search engine sunam       search engine sunam      search engine sunam-->

  <!-- Stylesheets -->
        <link href="<?php echo base_url() ?>css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>css/owl.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>css/flaticon.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>css/style.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>css/masterslider.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>css/site.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>css/jquery.simplyscroll.css" rel="stylesheet" type="text/css" />
        
        <!-- Responsive -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <link href="<?php echo base_url() ?>css/responsive.css" rel="stylesheet">
        <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
        <script>
        
      
            /*! BEGIN HTML5 SHIV SCRIPT */
            /* @preserve HTML5 Shiv v3.6.2pre | @afarkas @jdalton @jon_neal @rem | MIT/GPL2 Licensed
            */
    (function(n,t){function p(n,t){var i=n.createElement("p"),r=n.getElementsByTagName("head")[0]||n.documentElement;return i.innerHTML="x<style>"+t+"<\/style>",r.insertBefore(i.lastChild,r.firstChild)}function c(){var n=r.elements;return typeof n=="string"?n.split(" "):n}function o(n){var t=h[n[s]];return t||(t={},e++,n[s]=e,h[e]=t),t}function l(n,r,u){if(r||(r=t),i)return r.createElement(n);u||(u=o(r));var f;return f=u.cache[n]?u.cache[n].cloneNode():y.test(n)?(u.cache[n]=u.createElem(n)).cloneNode():u.createElem(n),f.canHaveChildren&&!v.test(n)?u.frag.appendChild(f):f}function w(n,r){if(n||(n=t),i)return n.createDocumentFragment();r=r||o(n);for(var f=r.frag.cloneNode(),u=0,e=c(),s=e.length;u<s;u++)f.createElement(e[u]);return f}function b(n,t){t.cache||(t.cache={},t.createElem=n.createElement,t.createFrag=n.createDocumentFragment,t.frag=t.createFrag());n.createElement=function(i) {
                                return r.shivMethods ? l(i, n, t) : t.createElem(i)
                            };
                            n.createDocumentFragment = Function("h,f", "return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&(" + c().join().replace(/\w+/g, function (n) {
                                return t.createElem(n), t.frag.createElement(n), 'c("' + n + '")'
                            }) + ");return n}")(r, t.frag)
                        }
                        function a(n) {
                                    n || (n = t);
                                    var u = o(n);
                                    return!r.shivCSS || f || u.hasCSS || (u.hasCSS = !!p(n, "article,aside,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}")), i || b(n, u), n
                                }
                                var u = n.html5 || {}, v = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i, y = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i, f, s = "_html5shiv", e = 0, h = {}, i, r;
                                (function () {
                                    try {
                                        var n = t.createElement("a");
                                        n.innerHTML = "<xyz><\/xyz>";
                                        f = "hidden"in n;
                                        i = n.childNodes.length == 1 || function () {
                                            t.createElement("a");
                                            var n = t.createDocumentFragment();
                                            return typeof n.cloneNode == "undefined" || typeof n.createDocumentFragment == "undefined" || typeof n.createElement == "undefined"
                                        }()
                                    } catch (r) {
                                        f = !0;
                                        i = !0
                                    }
                                })();
                                r = {elements: u.elements || "abbr article aside audio bdi canvas data datalist details figcaption figure footer header hgroup main mark meter nav output progress section summary time video", version: "3.6.2pre", shivCSS: u.shivCSS !== !1, supportsUnknownElements: i, shivMethods: u.shivMethods !== !1, type: "default", shivDocument: a, createElement: l, createDocumentFragment: w};
                                n.html5 = r;
                                a(t)
                            })(this, document);
                /*! END HTML5 SHIV SCRIPT */
        </script>
        
        <!--[if lt IE 8]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
         <!--[if lt IE 8]><script src="<?php echo base_url()?>js/respond.js"></script><![endif]-->
        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--[if lt IE 9]><script src="<?php echo base_url()?>js/respond.js"></script><![endif]-->
       
        
        <!-- Start WOWSlider.com HEAD section -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>engine1/style1.css" />
        <script type="text/javascript" src="<?php echo base_url() ?>engine1/jquery.js"></script>
        <!-- End WOWSlider.com HEAD section -->
        
        
          <!-- The fav icon -->
    <link rel="shortcut icon" href="<?php echo base_url();?>images/fav.jpg">
    </head>

    <body>
        <div class="page-wrapper">

            <!-- Preloader -->
      

            <!-- Main Header -->
            <header class="main-header-up">
                <div class="header-top">

                    <div class="auto-container">
                        <div class="row clearfix">
                            <!--Logo-->
                      
                            
                       <a href="index.html" title="Ilmul Quran Muslim Academy">
                        <div class="col-md-6 col-sm-6 col-xs-12 logo ">

                                <img src="<?php echo base_url();?>images/logo1.gif" style="height: 80px; width: 80px;" alt="Ilmul_quran" title="Ilmul Quran Muslim Academy">
                                <div class="logotext">
                                <span class="span1 wow fadeInRight animated  " style=""> ILMUL QURAN MUSLIM ACADEMY</span> <br>
                                <div class="logotext-s wow   fadeInRight animated" style=""> عـلـم القـران مـسـلــم اكاد يـمـي</div>
                                </div>
                        </div>
                       </a>
                            
                            
                            
                            
                            
                            
                            
                            
<!--                            Logo
                            <div class="col-md-6 col-sm-12 col-xs-12 logo ">
                                <h1><a href="<?php echo base_url() ?>welcome/index.html">
                                        <img src="<?php echo base_url();?>images/logo1.gif" style="height: 80px; width: 80px;" alt="Ilmul_quran" title="Ilmul Quran Muslim Academy">
                                    <img src="<?php echo base_url();?>images/logof.png" style="height: 80px; width: 400px;" alt="Ilmul_quran" title="Ilmul Quran Muslim Academy">
                                    </a></h1>
                            </div>-->

                            <div class="col-lg-6 col-md-8 col-sm-12 header-top-infos pull-right">
                                <ul class="clearfix">
                                    <li>
                                        <div class="clearfix ">
                                            <a style="color: white;" title="Call Now"> <img src="<?php echo base_url();?>images/icons/phone.png" alt="">
                                                <p style="margin-top: 5px;"><b>Call Us Now</b> <br><b> 01926751883</b></p></a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="clearfix ">
                                            <a href="https://www.facebook.com/" style="color: white;" title="Learn Quran In Online">
                                                <img src="<?php echo base_url();?>images/icons/onlinelearn.png" alt="">
                                                <p><b>Learn Quran</b> <br>In Online </p>
                                            </a>
                                        </div>
                                        
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>

                </div>
            </header> 
            <!--Header Lower-->
            <header class="main-header">
                <div class="header-top"></div>
                <div class="header-lower">
                    <div class="auto-container">
                        <div class="row clearfix">
                            <!--Main Menu-->
                            <nav class="col-md-9 col-sm-12 col-xs-12 main-menu">
                                <div class="navbar-header">
                                    <!-- Toggle Button -->      
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>

                                <div class="navbar-collapse collapse clearfix">                                                                                              
                                    <ul class="navigation">
                                        <li class=""><a href="<?php echo base_url()?>welcome/index.html"  title="Home"><samp class="fa fa-home"></samp> Home  </a>
                                        <li><a href="<?php echo base_url()?>welcome/about.html" title="About Us">About Us</a></li>
                                        
                                        <li class="dropdown"><a href="#" title="Academics">Academics</a>
                                            <ul class="submenu"> 
                                               
                                                <?php 
                                                $j=0;
                                                   foreach ($academic as $v_aca)
                                                   {
                                                       $j++;
                                                 ?>
                                                <li><a href="<?php echo base_url().'welcome/academics/'.$v_aca->id.'.html'  ?>" title="<?php echo $v_aca->menu_title  ?>" > <?php echo $v_aca->menu_title  ?></a></li>
                                                 <?php 
                                                  if ($j==4){
                                                      break;  
                                                  }
                                                 
                                                   }
                                                ?>
                                                
                                                
                                            </ul>
                                        </li>
                                         <li class="dropdown"><a href="#" title="Facilities">Facilities </a>
                                            <ul class="submenu">
                                                <?php 
                                                $j=0;
                                                   foreach ($facilities as $v_aca)
                                                   {
                                                       $j++;
                                                 ?>
                                                <li><a href="<?php echo base_url().'welcome/facilities/'.$v_aca->id  ?>" title="<?php echo $v_aca->menu_title  ?>" > <?php echo $v_aca->menu_title  ?></a></li>
                                                 <?php 
                                                  if ($j==4){
                                                      break;  
                                                  }
                                                 
                                                   }
                                                ?>
                                                
                                            </ul>
                                        </li>
                                        <li class="dropdown"><a href="#" title="Admission"> Admission </a>
                                            <ul class="submenu">
                                                <?php 
                                                $j=0;
                                                   foreach ($admission as $v_aca)
                                                   {
                                                       $j++;
                                                 ?>
                                                <li><a href="<?php echo base_url().'welcome/admission/'.$v_aca->id  ?>" title="<?php echo $v_aca->menu_title  ?>" > <?php echo $v_aca->menu_title  ?></a></li>
                                                 <?php 
                                                  if ($j==4){
                                                      break;  
                                                  }
                                                 
                                                   }
                                                ?>
                                                
                                                
                                            </ul>
                                        </li>
                                      
                                        <li class="dropdown"><a href="#" title="Events"> Events </a>
                                            <ul class="submenu">
                                                <?php 
                                                $j=0;
                                                   foreach ($events as $v_aca)
                                                   {
                                                       $j++;
                                                 ?>
                                                <li><a href="<?php echo base_url().'welcome/events/'.$v_aca->id  ?>" title="<?php echo $v_aca->menu_title  ?>" > <?php echo $v_aca->menu_title  ?></a></li>
                                                 <?php 
                                                  if ($j==4){
                                                      break;  
                                                  }
                                                 
                                                   }
                                                ?>
                                                
                                            </ul>
                                        </li>
                                       
                                        
                                       
                                        <li><a href="<?php echo base_url()?>welcome/gallery" title="Gallery">Gallery</a></li>
                                        <li><a href="<?php echo base_url()?>welcome/contact" title="Contact Us">Contact Us</a></li>
                                   </ul>
                                </div>
                            </nav>
                            <!--Main Menu End-->

                            <!--Social Links-->
                            <div class="col-md-3 col-sm-12 col-xs-12 social-outer">
                                <div class="social-links text-right">
                                    <a href="" class="sunam hiddena twitter " title="Donate">Donate</a>
                                    <a href="#" class="fa fa-facebook-f " title="Facebook"></a>
                                    <a href="#" class="fa fa-twitter " title="Twitter"></a>
                                    <a href="#" class="fa fa-google-plus " title="Google+"></a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!--End Main Header -->
