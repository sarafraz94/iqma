
            <!--Contact Options-->
            <section class="contact-options">
                <div class="clearfix">
                    <ul class="info-box clearfix wow bounceInRight">
                        <li><a href="contact.html"><span class="fa fa-phone"></span> Contact Us</a> </li>
                        <li><a href="contact.html"><span class="fa fa-phone"></span> Contact Us</a> </li>
                    </ul>
                </div>
            </section>

            <!--Main Footer-->
            <footer class="main-footer">
                <!--Footer Upper-->
                <div class="footer-upper">
                    <div class="auto-container">
                        <div class="row clearfix">

                            <!--Footer Widget-->
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="footer-widget contact-widget">
                                    <h3>Contact Us</h3>
                                    <div class="text">
                                    Ilmul quran faundation 
                                    Ilmul quran faundation
                                    Ilmul quran faundation Ilmul quran faundation
                                    </div>
                                    <ul class="info">
                                        <li><strong>Email</strong> <a href="">ilmul_quran@email.com</a></li>
                                        <li><strong>Phone</strong> <a href="#">+49 123 456 789</a></li>
                                        <li><strong>Fax</strong> <a href="#">+49 123 456 789</a></li>
                                        <li><strong>Website</strong> <a href="http://www.iqma-bd.org">http://www.ilmul-quran.com</a></li>
                                    </ul>
                                </div>
                            </div>

                            <!--Footer Widget-->
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="footer-widget services-widget">
                                    <h3>Facilities</h3>
                                    <ul class="links">
                                        <li><a href="#"><i class="fa fa-check-circle"></i>Ilmul quran faundation</a></li>
                                        <li><a href="#"><i class="fa fa-check-circle"></i>Ilmul quran faundation</a></li>
                                        <li><a href="#"><i class="fa fa-check-circle"></i>Ilmul quran faundation</a></li>
                                        <li><a href="#"><i class="fa fa-check-circle"></i>Ilmul quran faundation</a></li>
                                        <li><a href="#"><i class="fa fa-check-circle"></i>Ilmul quran faundation</a></li>
                                    </ul>
                                </div>
                            </div>

                            <!--Footer Widget-->
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="footer-widget support-widget">
                                    <h3>Quick Link</h3>
                                    <ul class="links">
                                        <li><a href="#"><i class="fa fa-star"></i>Online Quran Learn</a></li>
                                        <li><a href="#"><i class="fa fa-star"></i>Home</a></li>
                                        <li><a href="#"><i class="fa fa-star"></i>About Us</a></li>
                                        <li><a href="#"><i class="fa fa-star"></i>Our Gallery</a></li>
                                        <li><a href="#"><i class="fa fa-star"></i>Help &amp; Support Center</a></li>
                                    </ul>
                                </div>
                            </div>

                            <!--Footer Widget-->
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="footer-widget newsletter-widget">
                                    <h3>Subscribe Now</h3>
                                    <form action="#" class="clearfix">
                                        <p><input type="text" placeholder="Your Name"></p>
                                        <p><input type="text" placeholder="Your Email"></p>
                                        <p><textarea placeholder="Your Message"></textarea></p>
                                        <p><button class="hvr-bounce-to-right" type="submit">Send Message</button></p>
                                    </form>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>


                <!--Footer Bottom-->
                <div class="footer-bottom">
                    <div class="auto-container">

                        <div class="row clearfix">
                            <div class="col-md-6 col-sm-12 col-xs-12"><div class="copyright">Copryright 2015 by Ilmul Quran Faundation || All rights reserved</div></div>

                            <div class="col-md-6 col-sm-12 col-xs-12 social-outer">
                                <div class="social-links text-right">
                                    <a href="blog-left-right.html" class="twitter">Donate</a>
                                    <a href="#" class="fa fa-facebook-f "></a>
                                    <a href="#" class="fa fa-twitter "></a>
                                    <a href="#" class="fa fa-google-plus "></a>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </footer>

        </div>
        <!--End pagewrapper-->

        <!--Scroll to top-->
        <div class="scroll-to-top"> </div>

        <!--<script src="<?php echo base_url();?>js/jquery.js"></script>--> 
        <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>js/bxslider.js"></script>
        <script src="<?php echo base_url();?>js/owl.carousel.min.js"></script>
        <script src="<?php echo base_url();?>js/jquery.fancybox.pack.js"></script>
        <script src="<?php echo base_url();?>js/wow.js"></script>
        <script src="<?php echo base_url()?>js/jquery.simplyscroll.min.js"></script>
        <script src="<?php echo base_url();?>js/bootstrap-portfilter.min.js"></script>
        <script src="<?php echo base_url();?>js/script.js"></script>
        
        
    </body>
</html>
