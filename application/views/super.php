 <?php  
 class Super_Admin extends CI_Controller{

 
//  ==================+++++++++++++++++++++++++++====================
//  notice  *****  notice  *****  notice  *****  notice  *****  notice 
//  ==================+++++++++++++++++++++++++++====================
    
    

     public function notice()
     {
     
            $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
           
            if ($this->input->post('submit')) {
                
                $this->form_validation->set_rules('menu_title', 'Manu Title', 'required|min_length[5]|max_length[50]');
                $this->form_validation->set_rules('sub_title', 'Sub Title', 'required|min_length[5]|max_length[50]');
                $this->form_validation->set_rules('title', 'Title', 'required|min_length[10]|max_length[100]');
                $this->form_validation->set_rules('description', 'Description', 'required|min_length[100]');
                
                $data=array();
                
                $data['type_name'] = $this->input->post('type_name', true);
                $data['type_id'] = $this->input->post('type_id', true);
                $data['menu_title'] = $this->input->post('menu_title', true);
                $data['sub_title'] = $this->input->post('sub_title', true);
                $data['title'] = $this->input->post('title', true);
                $data['description'] = $this->input->post('description', true);
               
                
                                 /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */
                                      $config['upload_path'] = 'uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '1000';  // in KB 
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
//		$config['encrypt_name'] = TRUE;   // from struct over flow
                                     $new_name = time().$_FILES["image"]['name'];
                                     $config['file_name'] = $new_name;
                
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		
		$fdata=array();
		
		

                                /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */
                
                
          if ( !$this->form_validation->run() &&  !$this->upload->do_upload('image')) {
               $error = array('error' => $this->upload->display_errors());
               $this->load->view('admin/inc/header');
                $this->load->view('admin/modules/notice/add_notice_form' ,$error);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
          elseif ( !$this->form_validation->run() ||  !$this->upload->do_upload('image')) {
               $error = array('error' => $this->upload->display_errors());
               $this->load->view('admin/inc/header');
                $this->load->view('admin/modules/notice/add_notice_form' ,$error);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
            
            
            else {
                $fdata = $this->upload->data();
                $data['image'] =  $fdata['file_name'];
                $this->db->insert('tbl_notice', $data);
                
                $sdata = array();
                $sdata['afae_message'] = 'You can save Successfully !!';
                $this->session->set_userdata($sdata);
                 redirect('super_admin/notice');
            }
            
            
            
            
            }
            
         $this->load->view('admin/inc/header');
        // $this->load->view('upload_form', array('error' => ' ' ));
         $this->load->view('admin/modules/notice/add_notice_form', array('error' => ' ') );
         $this->load->view('admin/inc/footer');
         
              
     }
     
     
     public function manage_notice(){
         $this->load->view('admin/inc/header');
         $data = array();
         $data['all_notice'] = $this->super_admin_model->select_all_notice();
         
         $this->load->view('admin/modules/notice/manage_notice', $data );
         $this->load->view('admin/inc/footer');
     }
     public function notice_edit($id)
     {
        
        
           $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
           
            if ($this->input->post('submit')) {
                
                $this->form_validation->set_rules('menu_title', 'Manu Title', 'required|min_length[5]|max_length[50]');
                $this->form_validation->set_rules('sub_title', 'Sub Title', 'required|min_length[5]|max_length[50]');
                $this->form_validation->set_rules('title', 'Title', 'required|min_length[10]|max_length[100]');
                $this->form_validation->set_rules('description', 'Description', 'required|min_length[100]');
                
                $data=array();
                
                
                $data['menu_title'] = $this->input->post('menu_title', true);
                $data['sub_title'] = $this->input->post('sub_title', true);
                $data['title'] = $this->input->post('title', true);
                $data['description'] = $this->input->post('description', true);
               
                
                                 /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */
                 if (!empty($_FILES["image"]['name'])){                           
                                      $config['upload_path'] = 'uploads/';
                                	$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '1024';  // in KB
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
//		$config['encrypt_name'] = TRUE;   // from struct over flow
                                      $new_name = time().$_FILES["image"]['name'];
                                      if ($this->input->post('image_old') != null){
                                       $new_name =  $this->input->post('image_old');
                                      }
                                     $config['file_name'] = $new_name;
                                     
                                     // delete privious file 
                                     
                                     $path='uploads/'.$new_name;
                                    
                                    
                                     // delete privious file   
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		
		$fdata=array();
		
		if ( !$this->form_validation->run() &&  !$this->upload->do_upload('image')) {
               $error = array('error' => $this->upload->display_errors());
               $this->load->view('admin/inc/header');
                $data = array();
                $data = $error;
                $data['notice_info']=  $this->super_admin_model->select_notice_info_by_id($id);
                $this->load->view('admin/modules/notice/edit_notice_form' ,$data);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
          elseif ( !$this->form_validation->run() ||  !$this->upload->do_upload('image')) {
                $error = array('error' => $this->upload->display_errors());
                $this->load->view('admin/inc/header');
                $data = array();
                $data = $error;
                $data['notice_info']=  $this->super_admin_model->select_notice_info_by_id($id);
                $this->load->view('admin/modules/notice/edit_notice_form' ,$data);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
            
            
            else {
                
                unlink($path); 
                $fdata = $this->upload->data();
                if (  $fdata['file_name'] != $this->input->post('image_old')){
                $data['image'] =  $fdata['file_name'];}
                $this->db->where('id', $id)->update('tbl_notice', $data);
                
                $sdata = array();
                $sdata['afae_message'] = 'You can edit Successfully !!';
                $this->session->set_userdata($sdata);
                
                 redirect('super_admin/manage_notice');
            }
                            
        
           } 
		
		

                                /* ------------ image upload ---------------  */
                                /* ------------ image upload ---------------  */
                
                
          if ( !$this->form_validation->run()) {
                $this->load->view('admin/inc/header');
                $data = array();
                $data=array('error' => ' ');
                $data['notice_info']=  $this->super_admin_model->select_notice_info_by_id($id);
                $this->load->view('admin/modules/notice/edit_notice_form' ,$data);
                $this->load->view('admin/inc/footer');
                return;  
              
            }
         
            
            else {
                
                $this->db->where('id', $id)->update('tbl_notice', $data);
                
                $sdata = array();
                $sdata['afae_message'] = 'You can save Successfully !!';
                $this->session->set_userdata($sdata);
                
                 redirect('super_admin/manage_notice');
            }
            
            
            
            
            }
            
            
         $this->load->view('admin/inc/header');
         $data = array();
         // $this->load->view('upload_form', array('error' => ' ' ));
         $data=array('error' => ' ');
         $data['notice_info']=  $this->super_admin_model->select_notice_info_by_id($id);
         $this->load->view('admin/modules/notice/edit_notice_form', $data );
         $this->load->view('admin/inc/footer');
         
         
     }
     public function notice_delete($id) {
        if ($id != '') {
            $query =  $this->db->get_where('tbl_notice',array('id' => $id));
            $row = $query->row();
            $picture = $row->image;
            unlink(realpath('uploads/'.$picture));
            
            $this->db->where('id', $id)->delete('tbl_notice');
            redirect('super_admin/manage_notice');
        }
    }
    
//  ==================+++++++++++++++++++++++++++====================
//  notice  *****  notice  *****  notice  *****  notice  
//  ==================+++++++++++++++++++++++++++====================
    
        public  function select_all_notice(){
        $this->db->select('*');
         $this->db->from('tbl_notice');
         $this->db->where('type_id', 3);
         $query_result = $this->db->get();
         $result = $query_result->result();
         return $result; 
    }
    
    public function select_notice_info_by_id($id){
        $this->db->select('*');
        $this->db->from('tbl_notice');
        $this->db->where('id', $id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        }