    <div class="sidebar-page">
        <div class="auto-container">
            <div class="row clearfix">

                <!-- Left Content -->
                <section class="left-content col-lg-8 col-md-7 col-sm-7 col-xs-12">              



                	
                    
                    <!-- Contact Form -->
                    <div class="contact-form">
                			
                        <div class="sec-title"><h3 class="skew-lines">Leave a Message</h3></div>
                        <div class="msg-text">Fill out all required fields to send a message. You have to login to your wordpress account to post any comment. Please don´t spam, thank you!</div>
                        
                        <!--Contact Form-->
                        <form id="contact-form" method="post" action="<?php echo base_url() ?>welcome/contact">
                            <div class="row clearfix">
                                
                                <div class="col-md-5 col-sm-12 col-xs-12">
                                    
                                    <div class="form-group">
                                        <label class="form-label">Name</label>
                                        <input type="text" name="username" value="" placeholder="Enter Your Name">
                                    </div>
                                     <?php echo form_error('username', '<div class="alert alert-danger">', '</div>'); ?>
                                    
                                    <div class="form-group">
                                        <label class="form-label">Email</label>
                                        <input type="email" name="email" value="" placeholder="Enter Your Email Address">
                                    </div>
                                    <?php echo form_error('email', '<div class="alert alert-danger">', '</div>'); ?>
                                    
                                    <div class="clearfix"></div>
                                    
                                    <div class="form-group">
                                        <label class="form-label">Subject</label>
                                        <input type="text" name="subject" value="" placeholder="Enter a Subject">
                                    </div>
                                    <?php echo form_error('subject', '<div class="alert alert-danger">', '</div>'); ?>
                                    
                                </div>
                                
                                <div class="col-md-7 col-sm-12 col-xs-12">
                                    
                                    <div class="form-group">
                                        <label class="form-label">Message</label>
                                        <textarea name="message" placeholder="Type Message Here"></textarea>
                                    </div>
                                    <?php echo form_error('message', '<div class="alert alert-danger">', '</div>'); ?>
                                    
                                    
                                </div>
                                
                            </div>
                            
                            <div class="form-group text-right">
                                <button type="submit" name="submit" class="hvr-bounce-to-right" value="submit"><span class="fa fa-envelope"></span> Send Message</button>
                            </div>
                            
                        </form>
                            
                	</div>
                    
                
            	</section>
                
            <div id="sidebar" class="col-md-3" style="float:right;">                 
                <div id="text-11" class="widget ">			

                        <div class=""><div align="center" class="">Any question about our work or your donation?</div>
                            <h3 align="center" class="">Call (UK) 0300 111 3001</h3>

                            <div align="center">calls are included in your free mobile and landline minutes</div>

                            <p align="center">Email:&nbsp; sunam@gmail.com


                            </p>

                            <img src="<?php echo base_url()?>images/resource/securities.png" alt="Securities"/><a href="/how-money-is-spent/"><img src="<?php echo base_url()?>images/resource/donationbanner.png" alt="Securities"/></a></div>
                    </div>

                
                 	</div>       	</div>  	</div>       	</div>
